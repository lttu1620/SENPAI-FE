<?php

/**
 * Page of model
 *
 * @package Model
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class Notice extends AppModel {

    public $name = 'Notice';
    public $table = 'notices';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author caolp
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'user_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'question_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Question can not empty'),
                )
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }
}
