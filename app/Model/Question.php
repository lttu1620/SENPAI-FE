<?php

/**
 * Page of model
 *
 * @package Model
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class Question extends AppModel {

    public $name = 'Question';
    public $table = 'questions';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'user_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            'category_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Category can not empty'),
                )
            ),
            'content' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Content can not empty'),
                )
            ),
            'to_univ' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('To university can not empty'),
                )
            ),
            'to_high' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('To high can not empty'),
                )
            ),
            'to_teacher' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('To teacher can not empty'),
                )
            )
        );
        if ($this->validates())
            return true;
        return false;
    }

}
