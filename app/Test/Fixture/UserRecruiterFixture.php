<?php
/**
 * UserRecruiterFixture
 *
 */
class UserRecruiterFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => '????ID'),
		'company_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '??ID'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '???ID'),
		'introduction_text' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '?????', 'charset' => 'utf8'),
		'thumbnail_img' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '???????', 'charset' => 'utf8'),
		'is_admin' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'is_approved' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'disable' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '?????'),
		'created' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'updated' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_id' => 1,
			'user_id' => 1,
			'introduction_text' => 'Lorem ipsum dolor sit amet',
			'thumbnail_img' => 'Lorem ipsum dolor sit amet',
			'is_admin' => 1,
			'is_approved' => 1,
			'disable' => 1,
			'created' => 1,
			'updated' => 1
		),
	);

}
