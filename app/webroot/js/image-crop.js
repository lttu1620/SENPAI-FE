var cropper;
var options =
{
    imageBox: '.imageBox',
    thumbBox: '.thumbBox',
    spinner: '.spinner',
    imgSrc: 'avatar.png'
}
$(document).on('change', '#UserImageUrl', function () {
    var reader = new FileReader();
    reader.onload = function(e) {
        options.imgSrc = e.target.result;
        cropper = new cropbox(options);
    }
    reader.readAsDataURL(this.files[0]);
    $('#edit-profile').hide();
    $('.cropped').html('');
    $('.customBox').show();
    this.files = [];
});
$(document).on('click', '#btnCrop', function () {
    var img = cropper.getDataURL()
    $('.cropped').html('<img src="'+img+'">');
});
$(document).on('click', '#btnZoomIn', function () {
    cropper.zoomIn();
});
$(document).on('click', '#btnZoomOut', function () {
    cropper.zoomOut();
});

$(document).on('click', '.c-close', function () {
    $('#edit-profile').show();
    $('.customBox').hide();
});

$(document).on('click', '#btnSave', function () {
    if($('.cropped img').length > 0){
        $('#ImageData').val($('.cropped img').attr('src'));
        $('#avatar img').attr('src',$('.cropped img').attr('src'));
        $('#edit-profile').show();
        $('.customBox').hide();
    }else{
        $('.cropped').html('<span>この画像をトリミングしてください</span>');
    }
});
//
//$(document).on('change', '#UserImageUrl', function () {
//
//    if (this.files && this.files.length > 0) {
//        var files = this.files;
//        var check = document.getElementById('pic');
//        if(check){
//            $('#surface').html('');
//        }
//        var img  = document.createElement("img");
//        img.id = "pic";
//        img.file = files[0];
//        document.getElementById('surface').appendChild(img);
//        canvas = document.getElementById('canv1');
//        canvas.width = $('#surface').width();
//        canvas.height = $('#surface').height();
//        ctx = canvas.getContext('2d');
//        var reader = new FileReader();
//        reader.onload = (function(x) { return function(e) {
//            x.src = e.target.result;
//            if (x.height >= x.width) {
//                x.height = $('#surface').height();
////                x.width *= canvas.height/x.height;
////                x.height=canvas.height;
//            } else {
//                x.width = $('#surface').width();
////                x.height *= canvas.width/x.width;
////                x.width= canvas.width;
//            }
////
//            console.log(x.width);
//            console.log(x.height);
//            ctx.clearRect(0,0, ctx.width, ctx.height);
//            ctx.drawImage(x, 0, 0, x.width, x.height);
//            jQuery('#pic').Jcrop({ aspectRatio: 1, onSelect: cropImage });
//        };
//        })(img);
//        reader.readAsDataURL(files[0]);
//        $('.customBox').show();
//    }
//
//});