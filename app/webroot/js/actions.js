$(function () {
    if (typeof urls.likeUrl != "undefined") {
        $(document).on('click', 'a[data-action=like]', function () {
            var temp = $(this).closest('.box-like');
            var view = $(this).data('view');
            if (typeof view == "undefined") {
                view = 0;
            }
            $.ajax({
                type: 'POST',
                url: urls.likeUrl,
                data: {question_id: $(this).data('id'), flag: $(this).data('flag'), view: view},
                success: function (data) {
                    try {
                        var error = JSON.parse(data);
                    } catch (ex) {
                        temp.html(data);
                    }
                }
            });
        });
    }
    if (typeof urls.favoriteUrl != "undefined") {
        $(document).on('click', 'a[data-action=favorite]', function () {
            var temp = $(this).closest('.box-favorite');
            var view = $(this).data('view');
            if (typeof view == "undefined") {
                view = 0;
            }
            $.ajax({
                type: 'POST',
                url: urls.favoriteUrl,
                data: {question_id: $(this).data('id'), flag: $(this).data('flag'), view: view},
                success: function (data) {
                    try {
                        var error = JSON.parse(data);
                    } catch (ex) {
                        temp.html(data);
                    }
                }
            });
        });
    }

    if (typeof urls.likeAnswer != "undefined") {
        $(document).on('click', 'a[data-action=answer-like]', function () {
            var temp = $(this).closest('.box-like');
            $.ajax({
                type: 'POST',
                url: urls.likeAnswer,
                data: {answer_id: $(this).data('id'), flag: $(this).data('flag')},
                success: function (data) {
                    try {
                        var error = JSON.parse(data);
                    } catch (ex) {
                        temp.html(data);
                    }
                }
            });
        });
    }
    $('#post-question, *[data-event=post]').on('click', function () {
        var link = $('#post-link').data('href');
        var login = $('#post-link').data('login');
        var param = window.location.search;
        if (login)
            $('#box').load(link + param, null, function () {
                if ($('#QuestionPostquestiondialogForm').length > 0) {
                    $('#QuestionPostquestiondialogForm').ajaxForm(
                        {
                            beforeSerialize:function(jqForm, options){
                                $('#content').val($('#question-contents').val());
                            },
                            beforeSubmit: function (arr, $form, options) {
                                var isError = false;
                                if (!$('#category_customid').val()) {
                                    $('.error').text(error_message.category_id).show();
                                    return false;
                                }
                                if (!$('#question-contents').val()) {
                                    $('.error').text(error_message.content).show();
                                    return false;
                                } else {
                                    $('#content').val($('#question-contents').val());
                                }
                                $('.overlay').show();
                            },
                            beforeSend: function () {
                                var percentVal = '0%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);
                            },
                            uploadProgress: function (event, position, total, percentComplete) {
                                var percentVal = percentComplete + '%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);

                            },
                            success: function () {
                                var percentVal = '100%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);
                            },
                            complete: function (xhr) {
                                console.log(xhr);
                                $('.progress').hide();
                                var res = JSON.parse(xhr.responseText);
                                if (res.result == 'error') {
                                    $('.error').text('').show();
                                    for (var key in res.msg) {
                                        $('.error').append(res.msg[key] + '<br>');
                                    }
                                } else {
                                    $('#box').html('');
                                    $('#post-progress').width(0 + '%');
                                    showDialog(messages.question);
                                }
                            }
                        }
                    );
                }
                if ($('#AnswerPostanswerForm').length > 0) {
                    $('#AnswerPostanswerForm').ajaxForm(
                        {
                            beforeSerialize:function(jqForm, options){
                                $('#content').val($('#question-contents').val());
                            },
                            beforeSubmit: function (e) {
                                var isError = false;
                                var count_img = $('#image-view .avatar').length;
                                if (!$('#question-contents').val() && count_img == 0) {
                                    $('.error').text(error_message.content).show();
                                    return false;
                                } else {
                                    $('#content').val($('#question-contents').val());
                                }

                                $('.overlay').show();
                            },
                            beforeSend: function () {
                                var percentVal = '0%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);
                            },
                            uploadProgress: function (event, position, total, percentComplete) {
                                var percentVal = percentComplete + '%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);

                            },
                            success: function () {
                                var percentVal = '100%';
                                $('#post-progress').width(percentVal)
                                $('#post-progress').html(percentVal);
                            },
                            complete: function (xhr) {
                                $('.progress').hide();
                                try {
                                    var res = JSON.parse(xhr.responseText);
                                    if (typeof res.result != 'undefined' && res.result == 'error') {
                                        $('.error').text('').show();
                                        for (var key in res.msg) {
                                            $('.error').append(res.msg[key] + '<br>');
                                        }
                                    } else {
                                        $('#box').html('');
                                        $('#post-progress').width(0 + '%');
                                        showDialog(messages.answer);
                                    }
                                } catch (ex) {
                                    $('#box').html('');
                                    $('#post-progress').width(0 + '%');
                                    showDialog(messages.answer);
                                }
                            }
                        }
                    );
                }
            });
        else
            window.location = link;
    });
//    $(document.body).on('click', function (e) {
//        var target = $('.questionBox');
//        if (!$(target).is(e.target) && $(target).has(e.target).length === 0)
//            $('#box').html('');
//    });
    $(document).on('click', '.close-box', function () {
        $('#box').html('');
    });

    $(document).on('change', '.upload-img', function () {
        var input = this;
        var uid = $(this).attr('id');
        if (input.files && input.files.length > 0) {
            var files = input.files;

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                if (!file.type.match('image'))
                    continue;
                var filename = file.name;
                var doBreak = false;
                var id = $(".file-name").length + 1;
                $(".file-name").each(function (index) {
                    if ($(this).data('name') == filename) {
                        doBreak = true;
                        return false;
                    }
                });
                if (doBreak)
                    return false;
                var reader = new FileReader();
                reader.onload = function (e) {
                    var css = rotateImage(getOrientation(e.target.result));
                    var html = '<div class="image-preview"><a href="javascript:;" data-id="' + uid + '" class="remove"><div class="close"></div></a>' +
                        '<img  src="' + e.target.result + '" style="'+css+'" class="avatar" width="70" height= "70">' +
//                            '<br><span class="file-name" data-name="' + filename + '">' + 'Image ' + id + '<span>'+
                        '<div>';
                    $('#image-view').append(html);
                    $(input).hide();
                    var uniq = 'id' + (new Date()).getTime();
                    $('#file-group').append('<input type="file" name="data[images][]"  id="' + uniq + '" class="form-control upload-img" placeholder="Image" ' +
                        ' accept="image/jpeg,image/pjpeg,image/png,image/gif">');
                };
                //Read the image
                reader.readAsDataURL(file);
            }
        }
    });
    $(document).on('change', '.upload-video', function () {
        var id = $(this).attr('id');
        var html = '<div class="image-preview"><a href="javascript:;" data-id="' + id + '" class="remove"><div class="close"></div></a><div class="video-placeholder"></div><div>';
        $('#image-view').append(html);
        var uniq = 'id' + (new Date()).getTime();
        $('#file-group').append('<input type="file" name="data[videos][]" id="' + uniq + '" class="form-control upload-video" placeholder="Video" ' +
            ' accept="video/mp4,video/flv">');
    });
    $(document).on('click', '.remove', function () {
        var id = $(this).data('id');
        $('#' + id).remove();
        $(this).closest('.image-preview').remove();
    });
});
function saveText() {
    var data = $('#question-content').html();
    data = data.replace(/(<div>|<br>)/igm, '\n');
    data = data.replace(/(<\/div>)/igm, '');
    data = data.replace(/(<span .+>I)/igm, '#_I');
    data = data.replace(/(<\/span>)/igm, '_#');
    data = data.replace(/ <a href="javascript:;" class="dismiss-tag"><i class="ion ion-close"><\/i><\/a>/igm, '');
    $('#content').val(data);
}
function rotateImage(ori) {
    var cssAttr = '', deg = '';
    switch (ori) {
        case 8:
            deg = -90;
            break;
        case 3:
           deg = 180;
            break;
        case 6:
            deg = 90;
            break;
    }
    if(deg){
        cssAttr = '-webkit-transform: rotate('+deg+'deg);' +
            '-moz-transform: rotate('+deg+'deg);' +
            '-o-transform: rotate('+deg+'deg);' +
            '-ms-transform: rotate('+deg+'deg);' +
            'transform: rotate('+deg+'deg);';
    }
    return cssAttr;
}
function getOrientation(imgDataURL) {
    var byteString = atob(imgDataURL.split(',')[1]);
    var orientaion = byteStringToOrientation(byteString);
    return orientaion;

    function byteStringToOrientation(img) {
        var head = 0;
        var orientation;
        while (1) {
            if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 218) {
                break;
            }
            if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 216) {
                head += 2;
            }
            else {
                var length = img.charCodeAt(head + 2) * 256 + img.charCodeAt(head + 3);
                var endPoint = head + length + 2;
                if (img.charCodeAt(head) == 255 & img.charCodeAt(head + 1) == 225) {
                    var segment = img.slice(head, endPoint);
                    var bigEndian = segment.charCodeAt(10) == 77;
                    if (bigEndian) {
                        var count = segment.charCodeAt(18) * 256 + segment.charCodeAt(19);
                    } else {
                        var count = segment.charCodeAt(18) + segment.charCodeAt(19) * 256;
                    }
                    for (i = 0; i < count; i++) {
                        var field = segment.slice(20 + 12 * i, 32 + 12 * i);
                        if ((bigEndian && field.charCodeAt(1) == 18) || (!bigEndian && field.charCodeAt(0) == 18)) {
                            orientation = bigEndian ? field.charCodeAt(9) : field.charCodeAt(8);
                        }
                    }
                    break;
                }
                head = endPoint;
            }
            if (head > img.length) {
                break;
            }
        }
        return orientation;
    }
}