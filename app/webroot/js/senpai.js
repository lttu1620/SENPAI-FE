/**
 *
 * Senpai javascript function for template
 * @package Webroot.js
 * @created 2015-03-25
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */

$(function () {
    $('.question-item').on('click',function(e){
       if(!$(e.target).hasClass('ico_like') &&
           !$(e.target).hasClass('ico_star')&&
           !$(e.target).hasClass('ico_three-point')&&
           !$(e.target).hasClass('btn-report') &&
           !$(e.target).hasClass('bottom') &&
           !$(e.target).hasClass('life')){
           if($('.popover').hasClass('pop-show')){
               $('.popover').removeClass('pop-show');
               return false;
           }
           if(typeof $(e.target).data('event') == "undefined"){
               var href = $(this).data('href');
               window.location = href;
           }
       }
    });
    $(document).on('click', '*[data-action=toggle]', function () {
        var target = $(this).data('target');
        var focusTarget = $(this).data('focus');
        if (target)
            if ($(target).hasClass('togged')){
                $(target).removeClass('togged');
                setTimeout(function() {$(focusTarget).focus(); }, 200);
            }
            else
                $(target).addClass('togged');
    });
    $(document).on('click', '*[data-event=popover]', function () {
       var target = $(this).parent().find('*[data-type=popoverbox]');
        $(target).addClass('pop-show');
    });

    $(document.body).click(function(e) {
        if($(e.target).hasClass('bgBlack')){
            if($('.profileBox').is(':visible')){
                $($('.profileBox')).hide();
                $(e.target).hide();
            }
        }
        if($(e.target).hasClass('popover'))
            e.preventDefault();
        else{
            $('.popover').removeClass('pop-show');
        }
    });

    //fixed body height
    FixedHeight();
    $(window).resize(function(){
        FixedHeight();
    });
    //end fixed body height
    $(document).on('click', '*[data-event=modal]', function () {
        if ($('*[data-modal=on]').length == 0) {
            var target = $(this).data('target');
            $('#box').html($(target).html()).attr('data-modal', 'on');
            $(target).remove();
        } else {
            $('*[data-modal=on]').show();
            $('#edit-profile').show();
            $('.bgBlack').show();

        }
    });
    $(document).on('click', '*[data-modal=on] .close', function () {
        $('*[data-modal=on]').hide();
    });
    $(document).on('click', '*[data-event=close]', function () {
        var target = $(this).data('target');
        $(target).hide();
        $('.overlay').hide();
    });
    $(document.body).on('click', function (e) {
        $('*[data-action=toggle]').each(function () {
            var target = $(this).data('target');
            if (!$(target).is(e.target) && $(target).has(e.target).length === 0)
                if (!$(target).hasClass('togged'))
                    $(target).addClass('togged');
        });
    });
    //report action
    $(document).on('click', '*[data-event=report]', function () {
        var href = $(this).data('href');
        var reported = $(this).data('reported');
        var cur = $(this);
        $(this).removeClass('pop-show');
        if(typeof reported != "undefined") return false;
        $.ajax({
            url: href,
            beforeSend: function () {
                $(this).attr('data-reported',true);
            },
            success: function (data) {
                $('#dialog .dialog-body').text(data);
                FixCenter('#dialog',false,true);
                $('#dialog').show();
                $('.overlay').show();
            }
        });
    });

    //profile
    $(document).on('click', '#avatar', function () {
        $('#UserImageUrl').click();
    });
    $(document).on('change', '#UserImageUrl', function () {
//        previewImage(this, $('#avatar img'))
    });
    $(document).on('keyup', '#name',function () {
        $('#UserName').val($(this).val());
    }).on('keydown', '#name', function (e) {
            $('.error').hide();
            if (e.which == 13)
                return false;
        });
    $(document).on('keyup', '#memo', function () {
        var data = $(this).val();
        $('#UserMemo').val(data);
    });
    $(document).on('click', '#btn-save', function () {
        $('#UserProfileForm').submit();
    });

    if ($('#UserProfileForm').length > 0) {
        $('#UserProfileForm').ajaxForm(
            {
                beforeSerialize:function(jqForm, options){
                    $('#UserName').val($('#name').val());
                    $('#UserMemo').val($('#memo').val());
                },
                beforeSubmit: function (e) {
                    var isError = false;
                    if (!$('#UserName').val()) {
                        $('.error').text(error_message.name).show();
                        return false;
                    }
                    $('.overlay').show();
                },
                beforeSend: function () {
                    var percentVal = '0%';
                    $('#post-progress').width(percentVal)
                    $('#post-progress').html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    $('#post-progress').width(percentVal)
                    $('#post-progress').html(percentVal);

                },
                success: function () {
                    var percentVal = '100%';
                    $('#post-progress').width(percentVal)
                    $('#post-progress').html(percentVal);
                },
                complete: function (xhr) {
                    $('.progress').hide();
                    var res = JSON.parse(xhr.responseText);
                    if (res.result == 'error') {
                        $('.error').text('').show();
                        for (var key in res.msg) {
                            $('.error').append(res.msg[key] + '<br>');
                        }
                    } else {
                        $('#post-progress').width(0 + '%');
                        $('#avartar-thumb').attr('src', $('#avatar img').attr('src'));
                        $('#user_name').text($('#name').val());
                        $('#user_memo').html($('#memo').val().replace(/\r?\n/g, '<br />'));
                        $('#ImageData').val();
                        $('*[data-modal=on]').hide();
                    }
                    $('.overlay').hide();

                }
            }
        );
    }


    //post question
    $(document).on('click', '.to-target', function () {
        var target = $(this).data('target');
        var obj = $(this);
        if (obj.hasClass('active')) {
            obj.removeClass('active');
            $(target).val('0');

        } else {
            obj.addClass('active');
            $(target).val('1');
        }
    });
    $(document).on('click', '.btn-category', function () {
        var value = $(this).data('id');
        var obj = $(this);
        $('.error').hide();
        $('.btn-category').each(function () {
            $(this).removeClass('active');
        });
        obj.addClass('active');
        $('#popupBox02').hide();
        $('.profileBox').show();
        $('#category_customid').val(value);
    });
    $(document).on('keyup,change', '#question-contents', function () {
        $('.error').hide();
        var data = $(this).val();
        data = data.replace(/(<div>|<br>)/igm, '\n');
        data = data.replace(/(<\/div>)/igm, '');
        $('#content').val(data);
    });
    $(document).on('click', '#btn-picture', function () {
        $('.upload-img:last').click();
    });
    $(document).on('click', '#btn-video', function () {
        $('.upload-video:last').click();
    });
    $(document).on('click', '*[data-event=submit]', function () {
        var target = $(this).data('target');
        $(target + ' input[type=submit]').click();
    });
    if($('#pages_login').length > 0){
        FixCenter('#login-box',true,true);
    }
    loadLazyImage('body');

});
var loadLazyImage = function (parent) {
    $(parent + ' .lazy-image').each(function () {
        var img = new Image();
        var src = $(this).data('src');
        var imgTag = $(this);
        if (typeof src != "undefined") {
            img.onload = function () {
                imgTag.attr('src', src);
                imgTag.hide();
                imgTag.removeClass('lazy-image');
                imgTag.show();
            };
            img.onerror = function () {
                src = src.replace(imageSize, '');
                imgTag.removeAttr('data-src');
                var img2 = new Image();
                img2.onload = function () {
                    imgTag.attr('src', src);
                    imgTag.hide();
                    imgTag.removeClass('lazy-image');
                    imgTag.show();
                };
                img2.src = src;
            };
            img.src = src;
        }
    })
};
var FixedHeight = function (){
    var body_h = $(document.body).height();
    var w_h = $(window).height() - $('#header').height() - 37;
    if(body_h < w_h){
        $(document.body).height(w_h);
    }
};
var FixCenter = function(obj,top, left){
    var window_w = $(document).width();
    var window_h = $(document).height();
    var obj_h = $(obj).height();
    var obj_w = $(obj).width();
    $(obj).css('position','absolute');
    if(top){
        var res_h = (((window_h - obj_h)/2)/window_h)*100;
        $(obj).css('top',res_h + '%');
    }
    if(left){
        var res_w = (((window_w - obj_w)/2)/window_w)*100;
        $(obj).css('left',res_w + '%');
    }
};
var showDialog = function(content){
    $('#dialog .dialog-body').text(content);
    FixCenter('#dialog',false,true);
    $('#dialog').show();
    $('.overlay').show();
};
var loading = false;
$(function () {
    $(window).scroll(function () { //detect page scroll
        if ($(window).scrollTop() + $(window).height() == $(document).height())  //user scrolled to bottom of the page?
        {
            $('.animation_image').show();
            var url = $('#pagination .next a').attr('href');
            if (typeof url != "undefined") {
                if (!loading)
                    $.ajax({
                        url: url,
                        beforeSend: function () {
                            loading = true;
                        },
                        success: function (data) {
                            loading = false;
                            var content = $($.parseHTML(data)).filter('#itemList01');
                            $('#pagination').remove();
                            $('#itemList01').append(content.find('ul').html());
                            loadLazyImage('#itemList01');
                            $('.animation_image').hide();
                        }
                    }).fail(function () {
                            $('.animation_image').hide();
                        });
            } else {
                $('.animation_image').hide();
            }

        }
    });
    
    $('.category-item').on('click', function (e) {
        if (typeof $(e.target).data('event') == "undefined") {
            var href = $(this).data('href');
            window.location = href;
        }
    });
});