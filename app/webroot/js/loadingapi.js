/**
 *
 * Senpai javascript function for template
 * @package Webroot.js
 * @created 2015-03-25
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$(function(){
    $(document).on('submit','#UserLoginForm',function(e){
        e.preventDefault();
        $.ajax({
            url: login + location.search,
            data : $(this).serializeArray(),
            type: 'post',
            beforeSend : function(){
                $('#contentIndex').css('opacity',0);
                $('.splash-loading').show();
            },
            success: function(response){
                $('.splash-loading').hide();
                $('#contentIndex').replaceWith(response);
                var redirect = $('#contentIndex').data('redirect');
                var isupdate = $('#contentIndex').data('is_update');
                if(typeof  redirect != "undefined"){
                    FixCenter('.logo_app',true,true)
                    $('#contentIndex').css('opacity',1);
                    setTimeout(function(){
                        window.location.href = redirect;
                    },5000);
                }
                if(typeof isupdate != "undefined"){
                    FixCenter('#login-box',true,true)
                    $('#contentIndex').css('opacity',1);
                }
            }
        });
    });
    if(location.search.length > 0){
        $.ajax({
            url: login + location.search,
            beforeSend : function(){

            },
            success: function(response){
                $('.splash-loading').hide();
                $('#contentIndex').css('opacity',0);
                $('#contentIndex').replaceWith(response);
                var redirect = $('#contentIndex').data('redirect');
                var isupdate = $('#contentIndex').data('is_update');
                if(typeof  redirect != "undefined"){
                    FixCenter('.logo_app',true,true)
                    $('#contentIndex').css('opacity',1);
                    setTimeout(function(){
                        window.location.href = redirect;
                    },5000);
                }
                if(typeof isupdate != "undefined"){
                    FixCenter('#login-box',true,true)
                    $('#contentIndex').css('opacity',1);
                }
            }
        });
    }
});