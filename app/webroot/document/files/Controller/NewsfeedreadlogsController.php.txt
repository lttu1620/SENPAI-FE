<?php

/**
 * NewsFeedLikesController class of NewsFeedLikes Controller
 *
 * @package	Controller
 * @copyright Oceanize INC
 */
class NewsfeedreadlogsController extends AppController {

     /**
     * Initializes components for NewsfeedreadlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Newsfeedreadlogs.
     * 
     * @return void
     */
    public function index() {
        include ('Newsfeedreadlogs/index.php');
    }

}

