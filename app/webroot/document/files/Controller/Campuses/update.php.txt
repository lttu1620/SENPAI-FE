<?php

$modelName = $this->Campuse->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add campus');
if (!empty($id)) {
    $pageTitle = __('Edit campus');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_campuses_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/campuses',
        'name' => $pageTitle,
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// create update form
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('Id'),
    ))
    ->addElement(array(
        'id' => 'university_id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'university_name',
        'label' => __('University'),
        'options' => array(
            'url' => "/ajax/autocompleteuniversity",
            'callback' => "callbackCampuse",
        ),
        'onblur' => "onblurUniversity(this,'university_id');",
        'autocomplete_ajax' => true,
        'required' => true
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'address',
        'label' => __('Address'),
    ))
    ->addElement(array(
        'id' => 'access_method',
        'label' => __('Access method'),
    ))
    ->addElement(array(
        'id' => 'prefecture_id',
        'type' => 'input',
        'label' => __('Prefecture ID'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_campuses_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}
