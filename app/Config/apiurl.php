<?php
Configure::write('API.Timeout', 30);


Configure::write('API.url_users_list', 'users/list');
Configure::write('API.url_users_detail', 'users/detail');
Configure::write('API.url_users_disable', 'users/disable');
Configure::write('API.url_users_addupdate', 'users/addupdate');
Configure::write('API.url_users_forgetpassword', 'users/forgetpassword');
Configure::write('API.url_users_registeremail', 'users/registeremail');
Configure::write('API.url_users_registeractive', 'users/registeractive');
Configure::write('API.url_users_login', 'users/login');
Configure::write('API.url_users_login_facebook', 'users/fblogin');
Configure::write('API.url_users_registercompany', 'users/registercompany');
Configure::write('API.url_users_token', 'users/token');
Configure::write('API.url_users_resendregisteremail', 'users/resendregisteremail');
Configure::write('API.url_users_resendregistercompany', 'users/resendregistercompany');
Configure::write('API.url_users_resendforgetpassword', 'users/resendforgetpassword');

Configure::write('API.url_questions_top','questions/top');
Configure::write('API.url_questions_list','questions/list');
Configure::write('API.url_questions_add','questions/add');
Configure::write('API.url_questions_detail','questions/detail');

Configure::write('API.url_question_like','questionnices/add');
Configure::write('API.url_question_unlike','questionnices/disable');
Configure::write('API.url_question_favorite','questionfavorites/add');
Configure::write('API.url_question_unfavorite','questionfavorites/disable');

Configure::write('API.url_answers_add','answers/add');
Configure::write('API.url_answers_detail','answers/detail');

Configure::write('API.url_answers_like','answernices/add');
Configure::write('API.url_answers_unlike','answernices/disable');

Configure::write('API.url_categories_list','categories/list');
Configure::write('API.url_categories_detail','categories/detail');

Configure::write('API.url_fe_usersettings_list','fe/usersettings/list');

Configure::write('API.url_upload_image', 'upload/image.json');
Configure::write('API.url_upload_image2', 'upload/image2.json');
Configure::write('API.url_upload_video', 'upload/video.json');

Configure::write('API.url_users_profile','users/profile');
Configure::write('API.url_users_detail','users/detail');
Configure::write('API.url_users_addupdate','users/addupdate');
Configure::write('API.url_categories_all','categories/all');

Configure::write('API.url_violationreports_add','violationreports/add');

Configure::write('API.url_notices_list','notices/list');
Configure::write('API.url_notices_isread','notices/isread');

Configure::write('API.sh_login','http://tegakidemo.ildev.net/api/login.php');
Configure::write('API.url_check_user_by_number','users/loginbynumber');