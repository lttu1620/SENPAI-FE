<?php

$user_id = $this->Auth->user()->id;
$param = $this->data;
$apiUrl = "{$param['controller']}/disable";
$updateParams = array();
if ($param['controller'] == 'users' && $param['action'] == 'setting') {
    $apiUrl = "usersettings/addupdate";
    $updateParams = array(
        'user_id' => $user_id,
        'value' => json_encode(array(
                                    array(
                                        'setting_id' => $param['id'],
                                        'value' => $param['disable']
                                    )
                               ))
    );
}
$result = Api::call($apiUrl, $updateParams);
if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
$this->Common->deleteCacheAfterDisable($param['controller']);
exit;
