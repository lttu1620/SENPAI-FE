<?php

/**
 * postquestiondialog action
 * 
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$user_id = $this->Auth->user()->id;
$this->loadModel('Question');
$modelName = $this->Question->name;
$model = $this->{$modelName};
$data = array();

if ($this->request->isAjax()) {

    if ($this->request->isPost()) {
        $result = array(
            'result' => 'error',
            'msg' => array('content'=>__('Content can not empty'))
        );
        if ($model->validateInsertUpdate($this->getData($modelName))) {
            $img_list = array();
            $number = 1;
            $this->request->data['images']  = array_filter($this->request->data['images']);
            foreach ($this->request->data['images'] as $image) {
                $res = $this->Image->upload2($image,'questions');
                if (is_string($res)) {
                    $img_list[] = array('name' => 'Image ' . $number, 'url' => $res, 'type' => 'image');
                }
                $number++;
            }
//            $number = 1;
//            $this->request->data['videos']  = array_filter($this->request->data['videos']);
//                foreach ($this->request->data['videos'] as $video) {
//                    $res = $this->Video->upload2($video);
//                    if (is_string($res)) {
//                        $img_list[] = array('name' => 'Video ' . $number, 'url' => $res, 'type' => 'video');
//                    }
//                    $number++;
//                }
            $model->data[$modelName]['medias'] = json_encode($img_list);
            $id = Api::call(Configure::read('API.url_questions_add'), $model->data[$modelName]);
            if (!empty($id) && !Api::getError()) {
                $result['result'] = 'success';
            }
            // if validation error from api, write log and set validation error
            AppLog::info("Can not update", __METHOD__, $this->data);
        }
        $result['msg'] =  $model->validationErrors;
        echo json_encode($result);
        die;
    } else {
        $cate_id = 0;
        if(isset($this->request->query['category_id'])){
            $cate_id = $this->request->query['category_id'];
            $this->set('category_id',$cate_id);
        }
//get category
        $cats = Api::call(Configure::read('API.url_categories_list'), array('sort'=>'sort-asc'), false, array());
        $this->Common->handleException(Api::getError());
        $this->set(compact('cats'));
// create update form
        $this->UpdateForm->setModelName($modelName)
                ->setData($data)
                ->addElement(array(
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user_id
                ))
                ->addElement(array(
                    'id' => 'category_id',
                    'type' => 'text',
                    'value' => $cate_id
                ))
                ->addElement(array(
                    'id' => 'content',
                    'type' => 'textarea',
                    'modelName' => $modelName,
                ))
                ->addElement(array(
                    'name' => 'image_upload',
                    'type' => 'element',
                ))
                ->addElement(array(
                    'id' => 'to_univ',
                    'type' => 'text',
                    'value' => '0'
                ))
                ->addElement(array(
                    'id' => 'to_high',
                    'type' => 'text',
                    'value' => '0'
                ))
                ->addElement(array(
                    'id' => 'to_teacher',
                    'type' => 'text',
                    'value' => '0'
                ))
                ->addElement(array(
                    'type' => 'submit',
                    'value' => __('Save'),
                    'class' => 'btn btn-primary pull-left',
        ));
    }
}

