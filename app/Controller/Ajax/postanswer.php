<?php

/**
 * postanswer action
 * 
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */

$user_id = $this->Auth->user()->id;
$this->loadModel('Answer');
$modelName = $this->Answer->name;
$model = $this->{$modelName};
$data = array();

if ($this->request->isAjax()) {
    if ($this->request->isPost()) {
        $error = array('result' => 'error', 'msg' => array());
        if ($model->validateInsertUpdate($this->getData($modelName))) {
            $img_list = array();
            $number = 1;
            $this->request->data['images'] = array_filter($this->request->data['images']);
            if(empty($this->request->data[$modelName]['content']) && count($this->request->data['images']) == 0){
                $model->validationErrors['content'] = 'Content or image can not empty';
            }else{
                foreach ($this->request->data['images'] as $image) {
                    $res = $this->Image->upload2($image,'answers');
                    if (is_string($res)) {
                        $img_list[] = array('name' => 'Image ' . $number, 'url' => $res, 'type' => 'image');
                    }
                    $number++;
                }
                $number = 1;
                $this->request->data['videos'] = array_filter($this->request->data['videos']);
                foreach ($this->request->data['videos'] as $video) {
                    $res = $this->Video->upload2($video);
                    if (is_string($res)) {
                        $img_list[] = array('name' => 'Video ' . $number, 'url' => $res, 'type' => 'video');
                    }
                    $number++;
                }
                $model->data[$modelName]['medias'] = json_encode($img_list);
                $id = Api::call(Configure::read('API.url_answers_add'), $model->data[$modelName]);
                if (!empty($id) && !Api::getError()) {
                    $error['result'] = 'success';
                    $data = Api::call(Configure::read('API.url_answers_detail'), array('id' => $id));
                    $getAnswer = true;
                    $this->set(compact('data','getAnswer'));
                }
                // if validation error from api, write log and set validation error
                AppLog::info("Can not update", __METHOD__, $this->data);
            }
        }
        $error['msg'] = $model->validationErrors;
        echo json_encode($error);
        die;
    } else {
        $this->UpdateForm->setModelName($modelName)
                ->setData($data)
                ->addElement(array(
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user_id
                ))
                ->addElement(array(
                    'id' => 'question_id',
                    'type' => 'hidden',
                    'value' => $question_id
                ))
                ->addElement(array(
                    'id' => 'content',
                    'type' => 'textarea',
                    'modelName' => $modelName,
                ))
                ->addElement(array(
                    'name' => 'image_upload',
                    'type' => 'element',
                ))
                ->addElement(array(
                    'type' => 'submit',
                    'value' => __('Save'),
                    'class' => 'btn btn-primary pull-left',
        ));
    }
}

