<?php

/**
 * report action
 *
 * @package Controller
 * @created 2015-04-13
 * @version 1.0
 * @author Tuancd
 * @copyright Oceanize INC
 */

if ($this->request->isAjax()) {
    $param = $this->data;
    $result = Api::call(Configure::read('API.url_notices_isread'), $param);
    if (!Api::getError()) {
    }
    // delete cache and reload 
    AppCache::delete(Configure::read('total_new_notification')->key . '_' . $this->AppUI->id);
    $this->set('total_new_notification', MasterData::total_new_notification());
}
die;