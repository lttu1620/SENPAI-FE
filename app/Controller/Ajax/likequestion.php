<?php

/**
 * likequestion action
 * 
 * @package Controller
 * @created 2015-03-17
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$modelName = $this->Ajax->name;
$user_id = $this->Auth->user()->id;
$question_id = $this->request->data('question_id');
$flag = $this->request->data('flag');
$view = $this->request->data('view');
$request = array(
    'user_id' => $user_id,
    'question_id' => $question_id,
);
$apiUrl = $flag == '1' ? Configure::read('API.url_question_like') : Configure::read('API.url_question_unlike');
$result = Api::call($apiUrl, $request);

$data = array(
    'flag' => $flag == '0' ? '1' : '0',
    'text' => $flag == '0' ? __('Like') : __('Unlike'),
    'id' => $question_id,
    'view' => $view
);

if (Api::getError()) {
    AppLog::info($apiUrl . " failed", __METHOD__, $request);
    echo '{"error":true}'; die;
} else {
    if ($result['status'] == false) {
        echo '{"error":true}'; die;
    } else {
        AppLog::info($apiUrl . " failed", __METHOD__, $request);
        $data['flag'] = $flag == '0' ? '1' : '0';
        $data['text'] = $flag == '0' ? __('Like') : __('Unlike');
    }
    $data['nice_count'] = $result['count'];
}
$this->set(compact('data'));
