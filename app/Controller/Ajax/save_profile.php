<?php
$this->loadModel('User');
$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;

if ($this->request->isPost()) {
    $result = array(
        'result' => 'error',
        'msg' => array('content'=>__('Content can not empty'))
    );
    if ($model->validateUserInsertUpdate($this->getData($modelName))) {
        //d($model->data[$modelName], 1);
        if(!empty($this->request->data['img_data'])){
            $param = array('data'=>$this->request->data['img_data']);
            $res = Api::call(Configure::read('API.url_upload_image2'),$param);
            $model->data[$modelName]['image_url'] = $res;
        }else{
//            if (isset($this->request->data['User']['image_url']['name'])) {
//                $res = $this->Image->upload2($this->request->data['User']['image_url']);
//                if (!is_string($res)) {
//                    $res = '';
//                }
//                $model->data[$modelName]['image_url'] = $res;
//            } else {
                unset($model->data[$modelName]['image_url']);
//            }
        }
        $id = Api::call(Configure::read('API.url_users_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $result['result'] = 'success';
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    $result['msg'] =  $model->validationErrors;
    echo json_encode($result);
    die;
}