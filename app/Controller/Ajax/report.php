<?php

/**
 * report action
 *
 * @package Controller
 * @created 2015-04-13
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */

if ($this->request->isAjax()) {
    $user_id = $this->Auth->user()->id;
    $params = array(
        'user_id'       => $user_id,
        'question_id'   => $id,
        'answer_id'     => $ans_id
    );
    $result = Api::call(Configure::read('API.url_violationreports_add'), $params);
    if (!empty($result) && !Api::getError()) {
    }
    echo __('Thank for your report !');
}
die;