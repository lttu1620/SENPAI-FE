<?php

/**
 * Users Controller.
 * 
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UsersController extends AppController{

     /**
     * Construct
     * 
     * @author truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Profile action.
     * 
     * @author truongnn 
     * @return void
     */
    public function profile() {
        include ('Users/profile.php'); 
    }
    
    /**
     * Detail action.
     * 
     * @author truongnn 
     * @return void
     */
    public function editprofile() {
        include ('Users/editprofile.php'); 
    }
    
    /**
     * Detail action.
     * 
     * @author truongnn 
     * @return void
     */
    public function setting() {
        include ('Users/setting.php'); 
    }
}
