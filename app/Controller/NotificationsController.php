<?php

/**
 * Notifications Controller
 * 
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class NotificationsController extends AppController {

    public $allowedActions = array(
    );
    
    /**
     * Initializes components for CompaniesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action index   
     *  
     * @author caolp 
     */
    public function index() {
        include ('Notifications/index.php');
    }
}