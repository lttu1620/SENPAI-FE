<?php

$this->layout = 'page';
$this->view = 'loading';

if ($this->request->is('ajax')) {
    $this->layout = 'ajax';
    if (   isset($this->request->query['number']) 
        && isset($this->request->query['password']) 
        && isset($this->request->query['date']) 
        && !isset($this->request->data['update'])
    ) {
        $param = array(
            'number' => $this->request->query['number'],
            'password' => $this->request->query['password'],
            'date' => $this->request->query['date'],
        );
        $data = Api::callOtherSite(Configure::read('API.sh_login'), $param);

        if (!Api::getError()) {
            $user = $this->startLoginByNumber($param);
            if ($user) {
                $this->dispatchEvent('User.afterLogin', $param);             
                $this->set('redirect_url', Router::url('/'));
                $this->view = 'splash';
            } else {
                $data['number'] = $this->request->query['number'];
                $this->set(compact('data'));
                unset($data['name']);
                $this->request->data['User'] = $data;
                $this->view = 'login';
            }
        } else {
            $this->view = 'errorlogin';
        }
    } else if (isset($this->request->data['update'])) {
        $param = $this->request->data('User');
        $param['from_sh'] = true;
        $result = Api::call(Configure::read('API.url_users_addupdate'), $param);
        if (!Api::getError()) {
            $this->createLoginSession($result);
            $this->set('redirect_url', $this->Auth->redirect());
            $this->view = 'splash';
        } else {
            $this->set('error', Api::getError());
            $this->set('data', $param);
            $this->view = 'login';
        }
    } else {
        $this->view = 'splash';
    }
}