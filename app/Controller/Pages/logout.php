<?php
$this->Cookie->httpOnly = true;
$this->Cookie->write('last_login_url', $this->AppUI->is_admin == 1 ? 'admins/login' : 'login');
if ($this->Auth->logout()) {
	$url = $this->Cookie->read('last_login_url');
	$loginUrl = !empty($url) ? $url : 'login';
    return $this->redirect('/'.$loginUrl);
}