<?php

/**
 * top action
 * 
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$this->setPageTitle(__('Home'));
$this->loadModel('Question');
$modelName = $this->Question->name;
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;

if($this->request->isAjax()){
    $this->layout = 'ajax';
}

// create search form
$this->SearchForm
        ->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->setAttribute('action', 'search')
        ->addElement(array(
            'id' => 'keyword',
            'label' => __('Keyword'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('login_user_id'=>$user_id,'status' => 1,'disable' => 0,'page' => 1, 'limit' => Configure::read('Config.pageSize') , 'sort' => 'created-DESC'));
list($total, $data) = Api::call(Configure::read('API.url_questions_list'), $param, false, array());

$this->Common->handleException(Api::getError());
$error = array(
    'content' => __('Content can not empty'),
    'category_id' => __('Category can not empty')
);

$this->set(compact('total', 'data','error'));
$this->set('limit', $param['limit']);
$this->set('use_footer',true);

