<?php

/**
 * Notifications Controller index 
 * 
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$this->setPageTitle(__('Notifications'));
$param = $this->getParams(
    array(
        'login_user_id' => $this->AppUI->id,           
        'limit' => Configure::read('Config.pageSize')
    )
);
// delete cache and reload
AppCache::delete(Configure::read('total_new_notification')->key . '_' . $this->AppUI->id);
$this->set('total_new_notification', MasterData::total_new_notification());

list($total, $data) = Api::Call(Configure::read('API.url_notices_list'), $param);
if (Api::getError()) {
    AppLog::info("API.url_notices_list failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$this->set(compact('data', 'total'));
$this->set('limit', $param['limit']);
