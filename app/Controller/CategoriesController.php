<?php

/**
 * Categories Controller.
 * 
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class CategoriesController extends AppController{

     /**
     * Construct
     * 
     * @author truongnn 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Index action.
     * 
     * @author truongnn 
     * @return void
     */
    public function index() {
        include ('Categories/index.php'); 
    }
}
