<?php
$this->setPageTitle(__('Category'));
//$data = Api::call(Configure::read('API.url_categories_all'), array('sort'=>'sort-asc'));
$data = MasterData::categories_all();
if (Api::getError()) {
    AppLog::info("API.url_categories_all failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$this->set('categories',$data);

