<?php

App::uses('AppController', 'Controller');

/**
 * AjaxController class of Ajax Controller
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AjaxController extends AppController {

    /** @var string $layout layout name for view */
    public $layout = "ajax";

    /**
     * Handles user interaction of view autocompleteuniversity Ajax.
     *
     * @return void
     */
    public function autocompleteuniversity() {
        $search = $this->getParam('q', '');
        $data = MasterData::universities_all_key_value();
        $search = strtolower($search);

        $result = array();
        foreach ($data as $key => $value) {
            if (strpos(strtolower($value), $search) !== false) {
                $result[] = array(
                    'id' => $key,
                    'label' => $value
                );
            }
        }
        echo $this->getParam('callback') . '(' . json_encode($result) . ')';

        exit;
    }

    /**
     * Handles user interaction of view autocomplete Ajax.
     *
     * @return void
     */
    public function autocomplete() {
        $search = $this->getParam('q', '');
        $data = array(
            array('id' => 1, 'label' => 'Thai'),
            array('id' => 2, 'label' => 'Quan'),
            array('id' => 3, 'label' => 'Manh'),
            array('id' => 4, 'label' => 'Kino'),
            array('id' => 5, 'label' => 'Dien'),
            array('id' => 6, 'label' => 'Tu'),
            array('id' => 7, 'label' => 'Tuan'),
            array('id' => 8, 'label' => 'Truong'),
            array('id' => 9, 'label' => 'Huy'),
            array('id' => 10, 'label' => 'Thang'),
            array('id' => 11, 'label' => 'Hieu'),
        );
        $search = strtolower($search);
        $result = array();
        foreach ($data as $item) {
            if (strpos(strtolower($item['label']), $search) !== false) {
                $result[] = $item;
            }
        }
        echo $this->getParam('callback') . '(' . json_encode($result) . ')';
        exit;
    }

    /**
     * Handles user interaction of view resendregisteremail Ajax.
     *
     * @return void
     */
    public function resendregisteremail() {
        include ('Ajax/resendregisteremail.php');
    }

    /**
     * Handles user interaction of view resendforgetpassword Ajax.
     *
     * @return void
     */
    public function resendforgetpassword() {
        include ('Ajax/resendforgetpassword.php');
    }
    
    /**
     * Handles user interaction of view postquestiondialog Ajax.
     *
     * @return void
     */
    public function postquestiondialog() {
        include ('Ajax/postquestiondialog.php');
    }
    /**
     * Handles user interaction of view likequestion Ajax.
     *
     * @return void
     */
    public function likequestion() {
        include ('Ajax/likequestion.php');
    }
    /**
     * Handles user interaction of view favoritequestion Ajax.
     *
     * @return void
     */
    public function favoritequestion() {
        include ('Ajax/favoritequestion.php');
    }
    /**
     * Handles user interaction of view categorylist Ajax.
     *
     * @return void
     */
    public function categorylist() {
        include ('Ajax/categorylist.php');
    }
    
    /**
     * Handles user interaction of view postanswer Ajax.
     *
     * @return void
     */
    public function postanswer($question_id = null) {
        include ('Ajax/postanswer.php');
    }
    /**
     * Handles user interaction of view likeanswer Ajax.
     *
     * @return void
     */
    public function likeanswer() {
        include ('Ajax/likeanswer.php');
    }

    /**
     * Handles user interaction of view save_profile Ajax.
     *
     * @return void
     */
    public function save_profile() {
        include ('Ajax/save_profile.php');
    }

    /**
     * Handles user interaction of view report Ajax.
     *
     * @return void
     */
    public function report($id = 0, $ans_id = 0) {
        include ('Ajax/report.php');
    }

    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function disable() {
        include ('Ajax/disable.php');
    }
    //
    /**
     * Handles user interaction of view disable Ajax.
     *
     * @return void
     */
    public function isreadnotice() {
        include ('Ajax/isreadnotice.php');
    }
}
