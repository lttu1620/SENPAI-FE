<?php

/**
 * view action
 * 
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */

$this->setPageTitle(__('Question'));
$modelName = $this->Question->name;
$model = $this->{$modelName};
if (isset($this->Auth->user()->id))
    $user_id = $this->Auth->user()->id;
else
    $user_id = 0;
$data = array();


$param = $this->getParams(
        array(
            'id' => $id,
            'status' => 1,
            'login_user_id' => $user_id,
            'answer' => 1,
            'page' => 1,
            'limit' => Configure::read('Config.pageSize')
        )
);
if($this->referer() == Router::url(array('controller' => 'notifications', 'action' => 'index'),true)) {
    //delete cache
    $param['update_notice'] = 1;
}


$data[$modelName] = Api::Call(Configure::read('API.url_questions_detail'), $param);
if (empty($data[$modelName])) {
    AppLog::info("User unavailable", __METHOD__, $param);
    throw new NotFoundException("Question unavailable", __METHOD__, $param);
}
list($total, $answers) = $data[$modelName]['answers'];
$error = array(
    'content' => __('Content and image can not empty'),
);

$this->set(compact('total', 'answers','error'));
$this->set('limit', $param['limit']);
$this->set(compact('data'));
$this->set('use_footer',true);
