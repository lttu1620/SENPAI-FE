<?php

/**
 * search action
 * 
 * @package Controller/Questions
 * @created 2015-03-17
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
$title = __('Search');
$modelName = $this->Question->name;
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;

// create search form
$this->SearchForm
        ->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->setAttribute('action', 'search')
        ->addElement(array(
            'id' => 'keyword',
            'label' => __('Keyword'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$keyword = '';
if ($this->request->query('keyword') != null) {
    $this->Session->write('from','home');
    $keyword = $this->request->query('keyword');
    $title = __('Search') . '：' . $keyword;
}
$param = $this->getParams(array('login_user_id' => $user_id, 'status'=> 1,'disable' => 0, 'content' => $keyword, 'page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_questions_list'), $param, false, array());
$this->Common->handleException(Api::getError());
$error = array(
    'content' => __('Content can not empty'),
    'category_id' => __('Category can not empty')
);
if(isset($param['category_id'])){
    $title = __('Category') . '：';
    if($total == 0){
        $res = Api::call(Configure::read('API.url_categories_detail'), array('id'=>$param['category_id']));
        if (Api::getError()) {
            AppLog::info("API.url_categories_detail failed", __METHOD__, array('id'=>$param['category_id']));
        }else{
            $title .= $res['name'];
        }
    }else{
        $title .= $data[0]['category_name'];
    }
}
if(count($this->request->query) == 0 || (!isset($this->request->query['keyword']) && !isset($this->request->query['category_id']))){
    $title =  __('Question') .'：'. __('All');
    if(isset($this->request->query['joined_user_id']))
        $title =  __('Question') .'：'. __('Joined questions');
    if(isset($this->request->query['user_id']))
        $title = __('Question') .'：'. __('My questions');
}
$this->setPageTitle($title);

$this->set(compact('total', 'data','error'));
$this->set('limit', $param['limit']);
$this->set('use_footer',true);
