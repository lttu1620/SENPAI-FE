<?php
$this->setPageTitle(__('Profile'));
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;
if (empty($user_id)) {
    AppLog::info("User not available", __METHOD__, $user_id);
    throw new NotFoundException("User not available", __METHOD__, $user_id);
}

$user_profile = Api::call(Configure::read('API.url_users_profile'), array('user_id'=>$user_id));

if (Api::getError()) {
    AppLog::info("API.url_users_profile failed", __METHOD__, $user_id);
    return $this->Common->setFlashErrorMessage(Api::getError());
}
$error = array(
    'name' => __('Name can not empty'),
);
$this->set(compact('user_profile','error'));

