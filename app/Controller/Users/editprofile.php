<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;
// process when submit form
if ($this->request->is('post')) {
    if ($model->validateUserInsertUpdate($this->getData($modelName))) {
        //d($model->data[$modelName], 1);
        if ($this->request->data['User']['image_url']['name']) {
            $res = $this->Image->upload2($this->request->data['User']['image_url']);
            if (!is_string($res)) {
                $res = '';
            }
            $model->data[$modelName]['image_url'] = $res;
        } else {
            unset($model->data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_users_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/editprofile");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }

    $this->setFlashErrorMessage($this->User->validationErrors);
}
if (empty($user_id)) {
    AppLog::info("User not available", __METHOD__, $user_id);
    throw new NotFoundException("User not available", __METHOD__, $user_id);
} else {
    $data = Api::call(Configure::read('API.url_users_detail'), array('id' => $user_id));
    $this->Common->handleException(Api::getError());
}
$this->set('user', $data);
