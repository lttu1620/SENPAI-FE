<?php
$this->setPageTitle(__('Setting'));
$user_id = isset($this->Auth->user()->id) ? $this->Auth->user()->id : 0;
if (empty($user_id)) {
    AppLog::info("User not available", __METHOD__, $user_id);
    throw new NotFoundException("User not available", __METHOD__, $user_id);
}
$data = Api::call(Configure::read('API.url_fe_usersettings_list'), array('user_id'=>$user_id));
if (Api::getError()) {
    AppLog::info("API.url_fe_usersettings_list failed", __METHOD__, $param);
    return $this->Common->setFlashErrorMessage(Api::getError());
}

$this->set('setting',$data);

