<?php

/**
 * Questions Controller for test and reference only
 * 
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class QuestionsController extends AppController {

    public $allowedActions = array(
    );
    
    /**
     * Initializes components for CompaniesController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Action search
     *  
     * @author caolp 
     */
    public function search() {
        include ('Questions/search.php');
    }

    /**
     * Action view
     *  
     * @author caolp 
     */
    public function view($id) {
        include ('Questions/view.php');
    }

}
