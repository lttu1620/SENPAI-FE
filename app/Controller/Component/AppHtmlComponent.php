<?php

App::uses('AppComponent', 'Component');

/**
 * 
 * Set js/css for each page
 * @package Controller
 * @created 2014-11-24 
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class AppHtmlComponent extends AppComponent {

    /** @var array $__css List css */
    private $__css = array();
    
    /** @var array $__script List Js */
    private $__script = array();

    /**
     * Set js
     *    
     * @author thailvn
     * @param array $item Js information 
     * @return self
     */
    public function script($item) {
        $this->__script[] = $item;
        return $this;
    }

    /**
     * Set css
     *    
     * @author thailvn
     * @param array $item Css information
     * @return self
     */
    public function css($item) {
        $this->__css[] = $item;
        return $this;
    }

    /**
     * Get css
     *    
     * @author thailvn    
     * @return array List css
     */
    public function getCss() {        
        return $this->__css;
    }

    /**
     * Get js
     *    
     * @author thailvn    
     * @return array List Js
     */
    public function getScript() {
        return $this->__script;
    }

}
