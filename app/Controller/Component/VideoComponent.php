<?php  
 
App::uses('AppComponent', 'Component');
App::uses('Folder', 'Utility');

/**
 * 
 * Process upload video
 * @package Controller
 * @created 2014-11-27 
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class VideoComponent extends AppComponent { 
    
    /** @var array $components Use components */
    public $components = array('Common');
    
    /** @var array $allowed_mime_types Allowed mime types  */  
    public $allowed_mime_types = array( 
        'video/mp4', 
        'video/flv',   
    ); 
     
    /** @var array $allowed_extensions Allowed extensions  */ 
    public $allowed_extensions = array( 
        'mp4', 
        'flv',         
    );     
     
    /** @var object $controller Controller  */ 
    public $controller = null; 
     
    /** @var array $errorMsg Error message  */ 
    public $errorMsg = array(); 
    
    /** @var int $upload_maxsize Maxsize for uploading  */ 
    public $upload_maxsize = 0;  
     
    /**
     * Get instance 
     *    
     * @author thailvn      
     * @return self 
     */
    public static function getInstance()
    {
        static $_instance;
        if(empty($_instance)){
            $_instance = new self;
        }
        return $_instance;
    }
    
    /**
     * Init component with controller pointer 
     *    
     * @author thailvn
     * @param object $controller Controller     
     * @return void 
     */
    public function startup(Controller $controller) {
        $this->controller = &$controller;                
    }
     
     /**
     * Check video before upload
     *    
     * @author thailvn
     * @param array $file File information     
     * @return boolean 
     */
    public function checkUpload($file) {
        if (Configure::read('Config.upload_maxsize')) {
            $this->upload_maxsize = Configure::read('Config.upload_maxsize');  
        } else {
            $this->upload_maxsize = @ini_get('upload_max_filesize');
        }        
        if(array_key_exists('error', $file) AND $file['error'] === 0) {  
            $sizeByMB = ceil((int)$file['size']/1000000);
            $maxSizeByMB = intval(str_replace('M', '', $this->upload_maxsize));          
            if($file['size'] === 0 OR $sizeByMB > $maxSizeByMB) { 
                $this->errorMsg[] = __('Please upload a video <= %s', array($this->upload_maxsize));
                return false;                
            } elseif(!in_array($file['type'], $this->allowed_mime_types)) { 
                $this->errorMsg[] = __('Video type is invalid'); 
                return false;
            } else {                 
                $exploded = explode('.', $file['name']); 
                $extension = end($exploded);                 
                if (!in_array($extension, $this->allowed_extensions)) {
                    $this->errorMsg[] = __('Video extension is invalid'); 
                    return false;
                }
            }
            return true;
        }
        $this->errorMsg[] = __('Upload error');
        $this->errorMsg[] = json_encode($file);
        return false;
    }
    
    /**
     * Update a video
     *    
     * @author thailvn
     * @param array $file File information
     * @param string $thumb See more API upload/image
     * @return array|boolean
     */
    public function upload($field) {
        if(empty($field) OR $field === '') return false; 
        $exploded = explode('.', $field); 
        if (count($exploded) !== 2) return false; 
        list ($model, $value) = $exploded;
        if (array_key_exists($model, $this->controller->data) 
            AND array_key_exists($value, $this->controller->data[$model]) 
            AND is_array($this->controller->data[$model][$value])) {              
                $file = $this->controller->data[$model][$value];
                if ($this->checkUpload($file)) {                
                    return $this->_upload($file);                                  
                }
        } 
        return false;
    }      
   
    /**
     * Upload a image (for setting screen)
     *    
     * @author thailvn
     * @param array $file File information
     * @param string $thumb See more API upload/image
     * @return array|boolean
     */
    public function upload2($file) {        
        if ($file) {
            if ($this->checkUpload($file)) {                   
                return $this->_upload($file);                               
            } 
        } 
        return false;
    }  
     /**
     * Call API Upload a video
     *    
     * @author thailvn
     * @param array $file File information
     * @param string $thumb See more API upload/image
     * @return array|boolean
     */
    private function _upload($file) { 
        $filetype = $file['type'];
        $filename = $file['name'];
        $filedata = $file['tmp_name'];
        $filesize = $file['size'];
        if ($filedata != '')
        {
            $headers = array("Content-Type:multipart/form-data"); // cURL headers for file uploading            
            $cfile = new CurlFile($filedata, $filetype, $filename);
            $postfields = array(                
                'file' => $cfile
            );
            $url = Configure::read('API.Host') . Configure::read('API.url_upload_video');
            $ch = curl_init();
            $options = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_INFILESIZE => $filesize,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SAFE_UPLOAD => false,                        
                CURLOPT_SSL_VERIFYPEER => false,
            );
            curl_setopt_array($ch, $options);
            $jsonResponse = curl_exec($ch);               
            if ($error = curl_errno($ch)) {
                $errorDescription = curl_error($ch);
                curl_close($ch);
                AppLog::warning($errorDescription, __METHOD__, $file);
                throw new InternalErrorException($errorDescription, $error);
            }
            curl_close($ch);
            $result = json_decode($jsonResponse, true);
            if ($result['status'] == 500) {
                throw new InternalErrorException("Upload error", $result['status']);
            }                   
            if (count($result['body']) == 1) {
                return array_values($result['body'])[0];
            }
            return $result['body'];         
        }
        return false;
    }
} 
