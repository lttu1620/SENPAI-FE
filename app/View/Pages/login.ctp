<div id="contentIndex" data-is_update="true">
    <!--<div class="logo_app"><img src="img/img_main.png" alt="#" /></div>-->
    <div class="btn_group login">
        <div class="form-box bg-gray" id="login-box">
            <div class="header">初期登録</div>
            <div class="body bg-gray">

                <?php
                echo $this->Form->create('User',array('method' => 'post'));
                ?>
                <input type="hidden" name="update" value="1">
                <div class="form-group">
                    <strong class="f13px">senpaiでは、生活や勉強について質問することができます。まず登録時に使われる情報を登録します。</strong>
                </div>
                <?php
                echo $this->Form->hidden('number');
                echo $this->Form->hidden('user_group_id');
                echo $this->Form->hidden('enrollment_id');
                echo $this->Form->hidden('memo');
                ?>
                <span class="error">
                <?php
                if(isset($error['name']))
                    foreach($error['name'] as $s_e){
                        echo __($s_e) . '<br>';
                    }
                ?>
                </span>
                <?php
                echo $this->Form->input(
                    'name',
                    array (
                        'label' => false,
                        'class' => 'form-control',
                        'div'   => array ('class' => 'form-group'),
                        'placeholder'   => 'ニックネーム',
                    )
                );
                ?>
                <span class="error">
                <?php
                if(isset($error['grade']))
                    foreach($error['grade'] as $s_e){
                        echo __($s_e) . '<br>';
                    }
                ?>
                </span>
                <?php
                echo $this->Form->input(
                    'grade',
                    array (
                        'label' => false,
                        'class' => 'form-control',
                        'div'   => array ('class' => 'form-group'),
                        'placeholder'   => '学年',
                        'readonly' => isset($data['grade'])? true: false,
                        'value' => isset($data['grade'])?$data['grade']:''
                    )
                );
                echo $this->Form->input(
                    'sex_id',
                    array (
                        'label' => false,
                        'options' => array(
                            1 => '男性',
                            2 => '女性'
                        ),
                        'class' => 'form-control',
                        'div'   => array ('class' => 'form-group'),
                    )
                );
                ?>
                <div class="form-group submit">
                    <input value="登録する" class="btn bg-capture-red btn-block"
                           style="width:320px;" type="submit">
                </div>
                <?php
                echo $this->Form->end();
                ?>
                <div class="mB20"></div>
            </div>
        </div>
    </div>
</div>
