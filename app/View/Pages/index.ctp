<?php
$jsLinks = array(
    'likeUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'likequestion')),
    'favoriteUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'favoritequestion')),
);
$this->Html->scriptStart(array('inline' => false));
echo 'var urls = ' . json_encode($jsLinks) . ';';
$this->Html->scriptEnd();
$this->Html->script('actions', array('inline' => false));
?>
<div id="headingPage">
    <a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'search')) ?>"><span
            class="salmon active"><?php echo __('All')?></span></a>
    <a href="<?php echo $this->Html->url(
        array(
            'controller' => 'questions',
            'action' => 'search',
            '?' => array(
                'joined_user_id' => $this->Session->read('Auth.User')->id
            )
        )
    )?>"><span class="blueLight"><?php echo __('Joined questions')?></span></a>
    <a href="<?php echo $this->Html->url(
        array(
            'controller' => 'questions',
            'action' => 'search',
            '?' => array(
                'user_id' => $this->Session->read('Auth.User')->id
            )
        )
    )?>"><span
            class="blueLight"><?php echo __('My questions')?></span></a>
</div>
<div id="itemList01" class="data">
    <ul>
        <?php
        foreach ($data as $item) {
            echo $this->Item->questionItem($item);
        }
        ?>
        <li>
            <div id="pagination">
                <?php
                echo $this->Paginate->render($total, $limit);
                ?>
            </div>
        </li>
    </ul>
    <div class="animation_image" style="display:none" align="center">
        <img src="<?php echo $this->Html->webroot('img/loading.gif')?>">
    </div>

</div>
