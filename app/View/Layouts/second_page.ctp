<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <title><?php echo $meta['title'] . ' ｜ '. __('Senpai'); ?></title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="msApplication-PackageFamilyName" content="Microsoft.MoCamera_cw5n1h2txyevy"/>
    <meta name="msApplication-ID" content="Microsoft.Camera"/>
    <?php
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    echo $this->Html->css(array('style.css', 'base.css', 'custom'));
    echo $this->fetch('css');
    ?>
    <script type="text/javascript">
        var baseUrl = "<?php echo $this->html->url('/'); ?>";
        var controller = "<?php echo $controller; ?>";
        var action = "<?php echo $action; ?>";
        var referer = "<?php echo $referer; ?>";
        var url = "<?php echo $url; ?>";
        var imgBaseUrl = baseUrl + "<?php echo Configure::read('App.imageBaseUrl'); ?>";
        var imageSize = '<?php echo Configure::read('Config.imageDefaultSize')?>';
        var messages = {
            question: '<?php echo __('Question is posted, please wait for your post to be approved');?>',
            answer: '<?php echo __('Answer is posted, please wait for your post to be approved');?>'
        };
        <?php echo $this->Common->validateError() ?>
    </script>
</head>
<body>
<div id="wrapper">
    <!---- Header ---->
    <div id="header">
        <?php echo $this->element('header') ?>
    </div>
    <div id="searchBox" class="searchBox togged">
        <form action="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'search')); ?>">
            <input type="text" name="keyword" placeholder="<?php echo __('Search') ?>"/>
            <button type="submit" class="searchBoxIco"></button>
        </form>
    </div>
    <?php echo $this->element('nav') ?>
    <!---- Header ---->
    <div id="contentWrapper">
        <div id="contentInner">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
    <!---- footerNav ---->
    <?php if (isset($use_footer)) { ?>
        <div id="box"></div>
    <?php } ?>
    <footer>
        <?php echo $this->element('footer');?>
    </footer>
    <?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('jquery-ui.min.js');

    foreach ($moreScript as $script) {
        echo $this->Html->script($script);
    }
    echo $this->Html->script('bootstrap-toggle.js');
    echo $this->Html->script('common.js');
    echo $this->Html->script('senpai.js');
    echo $this->Html->script('jquery.form.js');

    echo $this->fetch('script');
    ?>
</div>
</body>
</html>
