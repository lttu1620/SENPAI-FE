<?php
if ($data['view'] == 0) {
    echo '<a href="javascript:;"  data-action="favorite" data-flag="' . $data['flag'] . '" data-id="' . $data['id'] . '">';
    if($data['flag'] == 0)
        echo '<div class="ico_star on"></div>';
    else
        echo '<div class="ico_star off"></div>';
    echo '</a>';
    echo '<span>' . intval($data['favorite_count']) . '</span>';
    echo '<span class="text">'.__('件').'</span>';
} else {
    $total_favorite = intval($data['favorite_count']) - 1;
    if ($data['flag'] == 0) {
        echo '<span><strong>' . __('You') . '</strong></span>';
        if ($total_favorite > 0) {
            echo '<span class="text">' . __('and') . '</span>';
            echo '<span><strong class="num-span">' . $total_favorite . '</strong></span>';
        }
        echo '<span class="text">';
        if ($total_favorite > 0) {
            echo __('people');
        }
        echo __('favorited this');
        echo '</span>';
        echo '<div class="min"><a href="javascript:;" data-action="favorite" data-flag="0" data-id="' . $data['id'] . '"data-view="1">' . $data['text'] . ' !</a></div>';
    } else {
        echo '<span><strong class="num-span">' . intval($data['favorite_count']) . '</strong></span>';
        echo '<span class="text">' . __('people') . __('favorited this') . '?></span>';
        echo '<div class="min">';
        echo '<a href="javascript:;" data-action="favorite" data-flag="1" data-id="' . $data['id'] . '" data-view="1">' . $data['text'] . ' !</a>';
        echo '</div>';
    }
}
