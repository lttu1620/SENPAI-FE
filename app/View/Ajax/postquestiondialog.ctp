<?php
/**
 * postquestiondialog action
 * 
 * @package View
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
?>
<div class="profileBox <?php if(!isset($category_id)) echo 'hide';?>">
    <ul>
        <li><div class="pCancle close-box">
                <div class="closeIcon"></div>
                <div class="closeText"><?php echo __('Cancel'); ?>
                </div>
            </div><div class="title"><?php echo __('Post question'); ?></div></li>
        <li class="border-bottom-none">
            <textarea class="qContent wP100" id="question-contents"></textarea>
        </li>
        <li class="border-bottom-none">
              <div id="image-view"></div>
        </li>
    </ul>
    <div class="capture"><div class="ico_capture"  id="btn-picture"></div></div>
    <div class="btn">
        <div class="three bgGray to-target border-right" data-target="#to_univ"><span class="ico_graduate"></span><span><?php echo __('Published in college students'); ?></span></div>
        <div class="three bgGray to-target border-right border-left" data-target="#to_high"><span class="ico_camera"></span><span><?php echo __('Published in high school students'); ?></span></div>
        <div class="three bgGray to-target border-left" data-target="#to_teacher"><span class="ico_heart"></span><span class="wP70"><?php echo __('Published in Counselors'); ?></span></div>
    </div>
    <div class="error" style="display: none"></div>
    <div class="hide">
        <?php
        echo $this->SimpleForm->render($updateForm);
        ?>
    </div>
    <div class="btn">
        <div class="one bgSalmon" data-event="submit" data-target="#QuestionPostquestiondialogForm">
            <div class="text">質問する</div></div>
        </div>
    </div>

</div>
<div id="popupBox02" class="<?php if(isset($category_id)) echo 'hide';?>">
    <div class="title"><?php echo __('Which category you want to publish?'); ?></div>
    <ul>
        <?php foreach ($cats[1] as $category) { ?>
            <li>
                <div class="thumbnail btn-category"  style="background-color:<?php echo $category['color'] . '!important'?>" data-id="<?php echo $category['id']; ?>"><img src="<?php echo $category['image_url']; ?>" alt="#"></div>
                <div class="text"><?php echo $category['name'];?></div>
            </li>
        <?php } ?>
    </ul>
    <div class="cancel close-box"><div class="btn-close-sm"></div><div class="btn-close-sm-text"> <?php echo __('Cancel'); ?></div></div>
</div>
<div class="bgBlack"></div>
<div class="overlay" style="display: none">
    <div class="progress">
        <div class="progress-bar progress-bar-info" id="post-progress" style="width: 0%">
            0%
        </div>
    </div>
</div>
