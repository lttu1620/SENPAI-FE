<?php
/**
 * postanswer action
 * 
 * @package View
 * @created 2015-03-19
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
?>
    <div class="profileBox">
        <ul>
            <li>
                <div class="pCancle close-box">
                    <div class="closeIcon"></div>
                    <div class="closeText"><?php echo __('Cancel'); ?>
                    </div>
                    </div>
                <div class="title"><?php echo __('Answer the question'); ?></div></li>
            <li class="border-bottom-none">
                <textarea class="qContent wP100" id="question-contents"></textarea>
            </li>
            <li class="border-bottom-none">
                <div id="image-view"></div>
            </li>
        </ul>
        <div class="btn">
            <div class="two bgGrayDark" id="btn-picture"><div class="ico_capture"></div></div>
            <div class="two bgSalmon" data-event="submit" data-target="#AnswerPostanswerForm"><div class="text"><?php echo __('Do answer')?></div></div>
        </div>
        <div class="error" style="display: none"></div>
        <div class="hide">
            <?php
            echo $this->SimpleForm->render($updateForm);
            ?>
        </div>
    </div>
    <div class="bgBlack"></div>
    <div class="overlay" style="display: none">
        <div class="progress">
            <div class="progress-bar progress-bar-info" id="post-progress" style="width: 0%">
                0%
            </div>
        </div>
    </div>
