<?php
if ($data['view'] == 0) {
    echo '<a href="javascript:;"  data-action="like" data-flag="' . $data['flag'] . '" data-id="' . $data['id'] . '">';
    if ($data['flag'] == 0)
        echo '<div class="ico_like on"></div>';
    else
        echo '<div class="ico_like off"></div>';
    echo '</a>';
    echo '<span>' . intval($data['nice_count']) . ' ' . __('件') . '</span>';
} else {
    $total_like = intval($data['nice_count']) - 1;
    if ($data['flag'] == 0) {
        echo '<span><strong>' . __('You') . '</strong></span>';
        if ($total_like > 0) {
            echo '<span class="text">' . __('and') . '</span>';
            echo '<span><strong class="num-span">' . $total_like . '</strong></span>';
        }
        echo '<span class="text">';
        if ($total_like > 0) {
            echo __('people');
        }
        echo __('liked this');
        echo '</span>';
        echo '<div class="min"><a href="javascript:;" data-action="like" data-flag="0" data-id="' . $data['id'] . '"data-view="1">' . $data['text'] . ' !</a></div>';
    } else {
        echo '<span><strong class="num-span">' . intval($data['nice_count']) . '</strong></span>';
        echo '<span class="text">' . __('people') . __('liked this') . '?></span>';
        echo '<div class="min">';
        echo '<a href="javascript:;" data-action="like" data-flag="1" data-id="' . $data['id'] . '" data-view="1">' . $data['text'] . ' !</a>';
        echo '</div>';
    }
}
