<?php
/**
 * postquestiondialog action
 * 
 * @package View
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo __('New question') ?></h4>

</div>
<div class="modal-body">
    <div class="te">
        <div class="row">
            <div class="col-md-12">    
                <div class="box box-primary" id="dialog-question">   
                    <div class="box-body"  id="camera-box">
                        <div>
                            <a href="javascript:;" class="btn" id="back-form"><i class="ion ion-arrow-left-a"></i></a>
                        </div>
                        <div class="col-md-12">
                            <div id="screen">No camera was detected.</div>
                        </div>
                        <div class="col-md-12">
                            <div class="camTop"></div>
                        </div>
                    </div>
                    <div class="box-body" id="form-box">              
                        <div class="alert alert-danger" style="display: none" role="alert" id="post-alert"></div>
                        <?php
                        echo $this->SimpleForm->render($updateForm);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
