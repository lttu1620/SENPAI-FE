<div class="form-group">
    <label for="<?php echo $data['id']; ?>"><?php echo $data['label']; ?><span class="input-required">*</span></label>
    <div id="template-div" class="textarea form-control"></div>
    <div id="question-content" class="textarea form-control" contenteditable></div>
    <div id="display">
    </div>
    <textarea name="data[<?php echo $data['modelName'] ?>][<?php echo $data['id'] ?>]" 
              class="form-control" 
              id="<?php echo $data['id']; ?>" 
              <?php echo $data['required'] ? 'required="true"' : '' ?> style="display: none"></textarea>
</div>