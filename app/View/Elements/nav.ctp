<!---- navPage ---->
<?php
$from = $this->Session->read('from');
?>
<div id="navPage">
    <ul>
        <li class="<?php
        if (
            $this->request->here == $this->Html->url(array('controller' => 'pages', 'action' => 'index'))
            || ($this->request->controller == 'questions' && $from == 'home')
        )
            echo 'active'
        ?>">
            <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index','?'=>array('from'=>'home'))) ?>">
                <div class="nav_02"></div>
                <div class="navText"><?php echo __('Home') ?></div>
            </a>
        </li>
        <li class="<?php
        if ($this->request->here == $this->Html->url(array('controller' => 'categories', 'action' => 'index')))
            echo 'active';
        else if($this->request->controller && $from == 'category')
            echo 'active'
        ?>">
            <a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'index','?'=>array('from'=>'category'))) ?>">
                <div class="nav_03"></div>
                <div class="navText"><?php echo __('Categories') ?></div>
            </a>
        </li>
        <li class="<?php
        if ($this->request->here == $this->Html->url(array('controller' => 'notifications', 'action' => 'index')))
            echo 'active';
        else if($this->request->controller && $from == 'notice')
            echo 'active'
        ?>">
            <a href="<?php echo $this->Html->url(array('controller' => 'notifications', 'action' => 'index','?'=>array('from'=>'notice'))) ?>">
                <div class="nav_04"></div>
                <div class="navText"><?php echo __('Notification') ?></div>
                <?php
                    if(!empty($total_new_notification) && intval($total_new_notification) > 0 ){
                        echo "<div class='num'>{$total_new_notification}</div>";
                    }
                ?>
            </a>
        </li>
        <li class="<?php
        if ($this->request->here == $this->Html->url(array('controller' => 'users', 'action' => 'profile')))
            echo 'active';
        else if($this->request->controller && $from == 'profile')
            echo 'active'
        ?>">
            <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'profile','?'=>array('from'=>'profile'))) ?>">
                <div class="nav_05"></div>
                <div class="navText"><?php echo __('Account') ?></div>
            </a>
        </li>
        <li class="<?php
        if ($this->request->here == $this->Html->url(array('controller' => 'users', 'action' => 'setting')))
            echo 'active'
        ?>">
            <a href="<?php
            if ($this->Session->check('Auth.User'))
                echo $this->Html->url(array('controller' => 'users', 'action' => 'setting','?'=>array('from'=>'setting')));
            else
                echo $this->Html->url(array('controller' => 'pages', 'action' => 'login'));
            ?>">
                <div class="nav_06"></div>
                <div class="navText"><?php echo __('Setting') ?></div>
            </a>
        </li>
    </ul>
</div>