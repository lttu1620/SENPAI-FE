<div id="post-link" data-href="<?php
if ($this->Session->check('Auth.User'))
    echo $this->Html->url(array('controller' => 'Ajax', 'action' => 'postanswer', $data['Question']['id']));
else
    echo $this->Html->url(array('controller' => 'pages', 'action' => 'login'));
?>" data-login="<?php echo $this->Session->check('Auth.User')?>"></div>
<div class="text"><?php echo __('Answer')?></div>
<div class="ico_men"></div>