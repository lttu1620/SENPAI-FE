<div class="navBack" onclick="window.history.back()"></div>
<div class="titleMain blueLight"><?php echo $meta['title']; ?></div>
<div class="search"  data-action="toggle" data-target="#searchBox"></div>
<!---- Search ---->
<div id="searchBox" class="searchBox togged">
    <form action="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'search')); ?>">
        <input type="text" name="keyword" placeholder="<?php echo __('Search') ?>" />
        <button type="submit"class="searchBoxIco"></button>    
    </form>
</div>
