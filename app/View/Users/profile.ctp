<?php
/**
 * profile action
 *
 * @package View
 * @created 2015-04-02
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */

$jsLinks = array(
    'likeUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'likequestion')),
    'favoriteUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'favoritequestion')),
);
$this->Html->scriptStart(array('inline' => false));
echo 'var urls = ' . json_encode($jsLinks) . ';';
$this->Html->scriptEnd();

$this->Html->script(array('crop-box','actions','image-crop'), array('inline' => false));
?>
<div id="listStyle01">
    <ul>
        <li class="liProfile">
            <div class="changeProfile" data-event="modal"
                 data-target="#profile-box"><?php echo __('Change profile') ?></div>
            <div class="avatar"><img
                    id="avartar-thumb"
                    src="<?php
                    if ($user_profile['profile']['image_url'])
                        echo $user_profile['profile']['image_url'];
                    else if($user_profile['profile']['sex_id'] == 1){
                        echo $this->Html->webroot('img/avatar_01.png');
                    }else{
                        echo $this->Html->webroot('img/avatar_02.png');
                    }?>"
                    alt=""/></div>
            <div class="content">
                <div
                    class="title" id="user_name"><?php echo !empty($user_profile['profile']['name']) ? $user_profile['profile']['name'] : ''; ?></div>
                <p class="des" id="user_memo"><?php echo !empty($user_profile['profile']['grade']) ? nl2br($user_profile['profile']['grade']) : ''; ?></p>
            </div>
            <div class="com01">
                <span class="num"><?php echo $user_profile['question_favorite_count']; ?></span>

                <div class="f11px"><?php echo __('Favorite count'); ?></div>
            </div>
            <div class="com02" style>
                <span class="num"><?php echo $user_profile['question_count']; ?></span>

                <div class="f11px"><?php echo __('Question count'); ?></div>
            </div>
            <div class="com03">
                <span class="num"><?php echo $user_profile['answer_count']; ?></span>

                <div class="f11px"><?php echo __('Answer count'); ?></div>
            </div>
        </li>

        <?php
        foreach ($user_profile['question_list'][1] as $item) {
            echo $this->Item->questionProfileItem($item);
        }
        ?>
    </ul>
</div>
<div class="hide">
    <form action="<?php
    echo $this->Html->url(
        array(
            'controller' => 'ajax',
            'action' => 'save_profile'
        )
    )
    ?>" enctype="multipart/form-data" id="UserProfileForm" method="post" accept-charset="utf-8">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="data[User][id]" value="<?php echo $user_profile['profile']["id"]?>" id="UserId">
        <div class="input file">
            <input type="file" name="data[User][image_url]" id="UserImageUrl">
        </div>
        <textarea name="data[img_data]" id="ImageData"></textarea>
        <input type="hidden" name="data[User][name]" value="<?php echo $user_profile['profile']["name"]?>" id="UserName">
        <input type="hidden" name="data[User][grade]" value="<?php echo $user_profile['profile']["grade"]?>" id="UserMemo">
    </form>
</div>
<div class="hide" id="profile-box">
    <div class="profileBox" id="edit-profile">
        <ul>
            <li>
                <div class="pCancle close">
                    <div class="closeIcon"></div>
                    <div class="closeText"><?php echo __('Cancel'); ?>
                    </div>
                </div>
                <div class="title"><?php echo __('Change profile') ?></div>
            </li>
            <li><span class="leftTextName"><?php echo __('Profile image') ?></span>

                <div class="avatar" id="avatar"><img src="<?php echo $user_profile['profile']['image_url'] ?>"
                                                     alt="avatar"></div>
            </li>
            <li><span class="leftText"><?php echo __('Given names'); ?></span>
                <input id="name" value="<?php echo $user_profile['profile']['name'] ?>">
            </li>
            <li><span class="leftText"><?php echo __('School year'); ?></span>
                <input id="memo" value="<?php echo $user_profile['profile']['grade']; ?>">
            </li>
        </ul>
        <div class="btn" id="btn-save">
            <div class="one"><?php echo __('Change') ?></div>
        </div>
        <div class="error hide"></div>
    </div>
    <div class="profileBox customBox hide">
        <div class="pCancle c-close">
            <div class="closeIcon"></div>
            <div class="closeText"><?php echo __('Cancel'); ?>
            </div>
        </div>
        <div class="imageBox">
            <div class="thumbBox"></div>
            <div class="spinner" style="display: none">Loading...</div>
        </div>
        <div class="action">
            <a class="btn-x btn-zoom-out" id="btnZoomOut"></a>
            <a class="btn-x btn-zoom-in" id="btnZoomIn"></a>
            <a class="btn-x btn-crop" id="btnCrop"></a>
            <a class="btn-x btn-save" id="btnSave"></a>
        </div>
        <div class="cropped">

        </div>
    </div>
    <div class="bgBlack"></div>
    <div class="overlay" style="display: none">
        <div class="progress">
            <div class="progress-bar progress-bar-info" id="post-progress" style="width: 0%">
                0%
            </div>
        </div>
    </div>
</div>
