<div id="itemList02">
    <div class="tList02"><?php echo __('Notification function'); ?></div>
    <ul>
        <?php foreach ($setting as $item): 
            ?>
            <li>
                <div class="title"><?php echo $item['description']; ?></div>
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch_<?php echo $item['setting_id']; ?>"
                           class="onoffswitch-checkbox toggle-event"
                           id="myonoffswitch_<?php echo $item['setting_id']; ?>"
                           <?php echo ($item['value'] == '1')? 'checked' : '' ;?>
                           value="<?php echo $item['setting_id']; ?>">
                    <label class="onoffswitch-label" for="myonoffswitch_<?php echo $item['setting_id']; ?>">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </li>
        <?php endforeach; ?> 
    </ul>
</div>