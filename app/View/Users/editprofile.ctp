<div id="itemSigle" id="edit-profile">
    <div class="avatar" id="avatar"><img src="<?php echo $user['image_url']?>" alt="#"></div>
    <div class="content">
        <div class="title" id="name" contenteditable="true"><?php echo $user['name']?></div>
        <div class="des" id="memo" contenteditable="true"><?php echo nl2br($user['memo']);?></div>
    </div>
    <div class="btn_group">
        <div class="hide">
            <?php 
            echo $this->Form->create('User', array("type" => "POST" , "enctype"=>"multipart/form-data")); 
            echo $this->Form->input('id', array("type" => "hidden","value" => $user["id"]));
            echo $this->Form->input('image_url', array("type" => "file","value" => $user["image_url"]));
            echo $this->Form->input('name', array("type" => "hidden","value" => $user["name"]));
            echo $this->Form->input('memo', array("type" => "textarea","value" => $user["memo"]));
            echo $this->Form->end();
            ?>
        </div>
        <div class="bt bgGrayDark" id="btn-save">保存</div>
    </div>
</div>

