<?php

/**
 * 
 * Some common helper
 * @package View.Helper
 * @created 2014-11-30
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class CommonHelper extends AppHelper {

    /** @var array $helpers Use helpers */
    public $helpers = array('Text', 'Form');

    /**
     * Truncate string
     *    
     * @author thailvn
     * @param string $text Input string
     * @param int $length Length
     * @param object $options See more String::truncate    
     * @return string  
     */
    function truncate($text, $length = 100, $options = array()) {
        return $this->getCommonComponent()->truncate($text, $length, $options);
    }

     /**
     * Date format for application
     *    
     * @author thailvn
     * @param int $time Input DateTime        
     * @return string Date
     */
    function dateFormat($time) {
        return $this->getCommonComponent()->dateFormat($time);
    }

     /**
     * Get thumb image url
     *     
     * @author thailvn
     * @param string $fileName File name
     * @param string $size Thumb size     
     * @return string Thumb image url  
     */
    function thumb($fileName, $size = null, $type = null) {
        return $this->getCommonComponent()->thumb($fileName, $size, $type);
    }

    /**
     * Render CKEditor script
     *     
     * @author thailvn
     * @param array $params Options         
     * @return string Html 
     */
    function editor($params = array()) {
        include_once WWW_ROOT . '/ckeditor/ckeditor_custom.php';
        $id = isset($params['id']) ? $params['id'] : '';
        $value = isset($params['value']) ? $params['value'] : '';
        $CKEditor = new CKEditor();
        $config = array();
        if (isset($params['width'])) {
            $config['width'] = $params['width'];
            unset($params['width']);
        }
        if (isset($params['height'])) {
            $config['height'] = $params['height'];
            unset($params['height']);
        }
        if (empty($config['height'])) {
            $config['height'] = 200;
        }
        $config['toolbar'] = array(
            array('Source'),
            array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'),
            array('Smiley', 'Table', 'Link', 'Unlink', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe'),
            array('Maximize', 'ShowBlocks'),
            array('Font', 'FontSize', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'TextColor', 'BGColor')
        );
        $config['filebrowserImageUploadUrl'] = "/imageupload.php";
        $config['filebrowserImageBrowseUrl'] = "/imagebrowser.php";
        $config['imageShowLinkTab'] = false;
        $CKEditor->basePath = '/ckeditor/';
        $params['type'] = 'textarea';
        $out = $CKEditor->editor($this->Form->input($id, $params), $id, $value, $config, null);
        return $out;
    }

    /**
     * Add image domain url for image url that is not image url
     *     
     * @author thailvn
     * @param string $imageUrl Image URL         
     * @return string Image URL 
     */
    public function paserImageUrl($imageUrl) {
        preg_match('/' . str_replace('/', '\/', Configure::read('Config.img_url')) . "/i", $imageUrl, $matches);
        if (empty($matches)) {
            return $imageUrl = Configure::read('Config.img_url') . $imageUrl;
        }
        return $imageUrl;
    }
    
    /**
     * Add validate message for js
     *     
     * @author caolp
     * @param    
     * @return string Image URL 
     */
    public function validateError() {
        if(isset($this->_View->viewVars['error']))
        {
            return 'var error_message = ' . json_encode($this->_View->viewVars['error']);
        }
        return '';
    }
}
