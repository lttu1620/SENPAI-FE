<?php

/**
 * 
 * ItemHelper Helper - render a item (news feed, comment, ...)
 * @package View.Helper
 * @created 2015-03-12
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class ItemHelper extends AppHelper {

    /** @var array $helpers Use helpers */
    public $helpers = array('Html');

    /**
     * Render a question item
     *     
     * @author thailvn   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function questionItem($item = array()) {
        return $this->fetch('questionItem', array('item' => $item));
    }
    /**
     * Render a question item for view
     *     
     * @author caolp   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function questionViewItem($item = array()) {
        return $this->fetch('questionViewItem', array('item' => $item));
    }
    /**
     * Render a question item for view
     *
     * @author caolp
     * @param array $item Item informaition
     * @return string Html
     */
    function questionProfileItem($item = array()) {
        return $this->fetch('questionProfileItem', array('item' => $item));
    }
    /**
     * Render a comment item
     *     
     * @author thailvn   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function answerItem($item = array()) {
        return $this->fetch('answerItem', array('item' => $item));
    }
    /**
     * Render time for question Item, Answer Item
     *
     * @author caolp
     * @param $date,$granularity
     * @return string
     */
    public function time_ago($date,$granularity=2) {

        $difference = time() - $date;

        $periods = array('decade' => 315360000,
            'year' => 31536000,
            'month' => 2628000,
            'week' => 604800,
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1);
        $retval='';
        foreach ($periods as $key => $value) {
            if ($difference >= $value) {
                $time = floor($difference/$value);
                $difference %= $value;
                $retval .= ($retval ? ' ' : '').$time.' ';
                $retval .= (($time > 1) ? __($key.'s') : __($key));
                $granularity--;
            }
            if ($granularity == '0') { break; }
        }
        return $retval. __(' ago');
    }

    /**
     * Render a comment item
     *     
     * @author tuancd   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function answer($item = array(),$message = "") {
        return $this->fetch('answer', array('item' => $item,'message'=> $message));
    }
    /**
     * Render a comment item
     *     
     * @author tuancd   
     * @param array $item Item informaition
     * @return string Html 
     */
    function favorite($item = array()) {
        return $this->fetch('favorite', array('item' => $item));
    }

    /**
     * Render a comment item
     *     
     * @author tuancd   
     * @param array $item Item informaition        
     * @return string Html 
     */
    function like($item = array()) {
        return $this->fetch('like', array('item' => $item));
    }
}
