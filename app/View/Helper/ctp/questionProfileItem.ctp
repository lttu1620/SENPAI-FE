<li class="bGreen question-item" style="border-color:<?php echo $item['color'] . '!important' ?> "  data-href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'view', $item['id'])) ?>">
    <div class="content">
        <a href="<?php echo $this->Html->url('/search?category_id=' . $item['category_id']) ?>">
            <div class="life green" style="color:<?php echo $item['color'] . '!important' ?> "><?php echo $item['category_name'] ?></div>
        </a>
        <div class="title"><?php echo $item['user_name']; ?></div>
        <p class="des"><?php echo nl2br($item['content']) ?></p>
        <?php
        foreach ($item['medias'] as $media) {
            ?>
            <div class="map">
                <?php
                if ($media['type'] == 'image') {
                    $img = $media['media_url'];
                    $size =  Configure::read('Config.imageDefaultSize');
                    $parts = explode('.', $img);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $img = $parts[0] . $size .'.'. $last;
                    echo '<img data-src="' . $img . '"  class="lazy-image" src="'. $this->Html->webroot('img/loading.gif').'">';
                } else {
                    ?>
                    <video controls>
                        <source src="<?php echo $media['media_url']; ?>">
                        Your browser does not support the video tag.
                    </video>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div class="bottom">
            <?php if ($this->Session->check('Auth.User')) { ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'view', $item['id'])) ?>">
                    <?php if (intval($item['answer_count']) > 0) { ?>
                        <div class="ico_com on"></div>
                    <?php } else { ?>
                        <div class="ico_com off"></div>
                    <?php } ?>
                </a><span><?php echo intval($item['answer_count']); ?> <?php echo __('件') ?></span>
                <div class="box-like">
                    <?php if (!$item['is_like']) { ?>
                        <a href="javascript:;" data-action="like" data-flag="1"
                           data-id="<?php echo $item['id']; ?>">
                            <div class="ico_like off"></div>
                        </a><span><?php echo intval($item['nice_count']) ?> <?php echo __('件') ?></span>
                    <?php } else { ?>
                        <a href="javascript:;" data-action="like" data-flag="0"
                           data-id="<?php echo $item['id']; ?>">
                            <div class="ico_like on"></div>
                        </a><span><?php echo intval($item['nice_count']) ?> <?php echo __('件') ?></span>
                    <?php } ?>
                </div>
                <div class="box-favorite">
                    <?php if (!$item['is_favorite']) { ?>
                        <a href="javascript:;" data-action="favorite" data-flag="1"
                           data-id="<?php echo $item['id']; ?>">
                            <div class="ico_star off"></div>
                        </a><span><?php echo intval($item['favorite_count']) ?> <?php echo __('件') ?></span>
                    <?php } else { ?>
                        <a href="javascript:;" data-action="favorite" data-flag="0"
                           data-id="<?php echo $item['id']; ?>">
                            <div class="ico_star on"></div>
                        </a><span><?php echo intval($item['favorite_count']) ?> <?php echo __('件') ?></span>
                    <?php } ?>
                </div>
                <div class="box-report">
                    <a href="javascript:;" data-event="popover"><div class="ico_three-point"></div></a>
                    <a class="popover btn-report" href="javascript:;"
                       data-event="report"
                       data-type="popoverbox"
                       title="<?php echo __('Report violation') ?>"
                       data-href="<?php echo $this->Html->url(array('controller' => 'ajax', 'action' => 'report', $item['id'])) ?>"></a>
                </div>
            <?php } else { ?>
                <a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'view', $item['id'])) ?>">
                    <?php if (intval($item['answer_count']) > 0) { ?>
                        <div class="ico_com on"></div>
                    <?php } else { ?>
                        <div class="ico_com off"></div>
                    <?php } ?>
                </a><span><?php echo intval($item['answer_count']); ?> <?php echo __('件') ?></span>
                <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'login')) ?>">
                    <div class="ico_like off"></div>
                </a><span><?php echo intval($item['nice_count']) ?> <?php echo __('件') ?></span>
                <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'login')) ?>">
                    <div class="ico_star off"></div>
                </a><span><?php echo intval($item['favorite_count']) ?> <?php echo __('件') ?></span>
            <?php } ?>
            <div class="min"><?php echo $this->Common->dateFormat($item['created']) ?></div>
        </div>
    </div>

</li>