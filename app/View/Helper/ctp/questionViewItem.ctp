<li class="question-view">
    <div class="avatar"><img src="<?php
        if ($item['user_image_url'])
            echo $item['user_image_url'];
        else if($item['sex_id'] == 1){
            echo $this->Html->webroot('img/avatar_01.png');
        }else{
            echo $this->Html->webroot('img/avatar_02.png');
        }
        ?>" alt="#"></div>
    <div class="content bGreen" style="border-color:<?php echo $item['color'] . '!important' ?> ">
        <a href="<?php echo $this->Html->url('/search?category_id=' . $item['category_id']) ?>">
            <div class="life green"
                 style="color:<?php echo $item['color'] . '!important' ?> "><?php echo $item['category_name'] ?></div>
        </a>

        <div class="title"><?php echo $item['user_name']; ?></div>
        <p class="des"><?php echo nl2br($item['content']) ?></p>
        <?php
        foreach ($item['medias'] as $media) {
            ?>
            <div class="map">
                <?php
                if ($media['type'] == 'image') {
                    $img = $media['media_url'];
                    $size =  Configure::read('Config.imageDefaultSize');
                    $parts = explode('.', $img);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $img = $parts[0] . $size .'.'. $last;
                    echo '<img data-src="' . $img . '"  class="lazy-image" src="'. $this->Html->webroot('img/loading.gif').'">';
                } else {
                    ?>
                    <video controls>
                        <source src="<?php echo $media['media_url']; ?>">
                        Your browser does not support the video tag.
                    </video>
                <?php
                }
                ?>
            </div>
        <?php
        }
        ?>
        <?php if ($this->Session->check('Auth.User')) { ?>

            <div class="view-report">
                <div class="box-report">
                    <a href="javascript:;" data-event="popover"><div class="ico_three-point"></div></a>
                    <a class="popover btn-report" href="javascript:;"
                       data-event="report"
                       data-type="popoverbox"
                       title="<?php echo __('Report violation') ?>"
                       data-href="<?php echo $this->Html->url(array('controller' => 'ajax', 'action' => 'report', $item['id'])) ?>"></a>
                </div>
            </div>
        <div class="bottom border">
            <div class="ico_like on"></div>
            <div class="box-like">
                <?php
                //call
                $total_like = intval($item['nice_count']);
                $set_and = true;
                if ($item['is_like']) {
                    $total_like = $total_like - 1;
                    echo '<span><strong>' . __('You') . '</strong></span>';
                    if ($total_like > 0) {
                        echo '<span class="text">' . __('and') . '</span><span><strong class="num-span">' . $total_like . '</strong></span>';
                    }
                } else {
                    echo '<span><strong class="num-span">' . $total_like . '</strong></span>';
                }
                echo '<span class="text">';

                if (!$item['is_like'] || $total_like > 0) {
                    echo __('people');
                }
                echo __('liked this');
                echo '</span><div class="min">';
                if (!$item['is_like']) {
                    echo '<a href="javascript:;" data-action="like" data-flag="1"
                               data-id="' . $item['id'] . '" data-view="1">' . __('Like') . ' !</a>';
                } else {
                    echo '<a href="javascript:;" data-action="like" data-flag="0"
                               data-id="' . $item['id'] . '" data-view="1">' . __('Unlike') . ' !</a>';
                }
                echo '</div>';
                ?>
        </div>
    </div>
    <div class="bottom border">
        <div class="ico_star on"></div>
        <div class="box-favorite">
            <?php
            //call
            $total_favorite = intval($item['favorite_count']);
            $set_and = true;
            if ($item['is_favorite']) {
                $total_favorite = $total_favorite - 1;
                echo '<span><strong>' . __('You') . '</strong></span>';
                if ($total_favorite > 0) {
                    echo '<span class="text">' . __('and') . '</span><span><strong class="num-span">' . $total_favorite . '</strong></span>';
                }
            } else {
                echo '<span><strong class="num-span">' . $total_favorite . '</strong></span>';
            }
            echo '<span class="text">';

            if (!$item['is_favorite'] || $total_favorite > 0) {
                echo __('people');
            }
            echo __('favorited this');
            echo '</span><div class="min">';
            if (!$item['is_favorite']) {
                echo '<a href="javascript:;" data-action="favorite" data-flag="1"
                               data-id="' . $item['id'] . '" data-view="1">' . __('Favorite') . ' !</a>';
            } else {
                echo '<a href="javascript:;" data-action="favorite" data-flag="0"
                               data-id="' . $item['id'] . '" data-view="1">' . __('Unfavorite') . ' !</a>';
            }
            echo '</div>';
            ?>
        </div>
    </div>
    <?php } else { ?>
        <div class="bottom border"><?php if (intval($item['nice_count']) > 0) { ?>
                <div class="ico_like on"></div>
            <?php } else { ?>
                <div class="ico_like off"></div>
            <?php } ?><span><strong><?php echo intval($item['nice_count']) ?></strong></span><span
                class="text"><?php echo __('people') . __('liked this') ?></span>

            <div class="min"><a
                    href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'login')) ?>"><?php echo __('Like') ?>
                    !</a></div>
        </div>
        <div class="bottom border"><?php if (intval($item['favorite_count']) > 0) { ?>
                <div class="ico_star on"></div>
            <?php } else { ?>
                <div class="ico_star off"></div>
            <?php } ?><span><strong><?php echo intval($item['favorite_count']) ?></strong></span><span
                class="text"><?php echo __('people') . __('favorited this') ?></span>

            <div class="min"><a
                    href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'login')) ?>"><?php echo __('Favorite') ?>
                    !</a></div>
        </div>
    <?php } ?>
    </div>
</li>

