 <li class="sty02<?php if (empty($item['is_read'])) echo ' unread'?>">
  <a href="<?php echo $this->Html->url(array('controller'=>'questions','action'=>'view',$item['question_id'])) ?>" 
  class="isread_notice" data-id="<?php echo $item['id']?>">
    <div class="left"><div class="avatar_l"><img alt="#" src="<?php echo $item['user_image_url'];?>"></div></div>
    <div class="content">
        <div><strong><?php echo $item['user_name'] ?></strong><?php echo $message;?></div>
        <!-- 
        <p class="note"><?php echo $item['questions_content']; ?></p> </br>
        <p class="note"><?php echo $item['answers_content']; ?></p> -->
    </div>
    <div class="date">
        <span>  
            <?php  echo $this->Common->dateFormat($item['updated'])?>
        </span>
     </div>
   </a>
</li>