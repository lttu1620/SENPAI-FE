<li class="sty01<?php if (empty($item['is_read'])) echo ' unread'?>">
    <a href="<?php echo $this->Html->url(array('controller'=>'questions','action'=>'view',$item['question_id'])) ?>" 
    class="isread_notice" data-id="<?php echo $item['id']?>">
        <div class="left"><span class="icon icon_like"></span></div>
        <div class="content">
          <!-- <div class="avatarGroup">
              <div class="avatar_s"><img alt="#" src="common/img/avatar_01.png"></div>
                <div class="avatar_s"><img alt="#" src="common/img/avatar_01.png"></div>
                <div class="avatar_s"><img alt="#" src="common/img/avatar_01.png"></div>
                <div class="avatar_s"><img alt="#" src="common/img/avatar_01.png"></div>
                <div class="avatar_s"><img alt="#" src="common/img/avatar_01.png"></div>
            </div> -->
            <p>
                <?php if (empty($item['answer_id'])) { ?>
                         <?php if(intval($item['nice_count']) > 1 ){ ?>
                            <strong><?php echo $item['user_name'] ?></strong>さんと他 <?php echo (intval($item['nice_count']) - 1);?>人があなたの質問をいいね！しました。
                        <?php } else {?>
                            <strong><?php echo $item['user_name'] ?></strong>さんがあなたの質問をいいね！しました。
                        <?php }
                    }else { ?>
                        <?php if(intval($item['nice_count']) > 1 ){ ?>
                            <strong><?php echo $item['user_name'] ?></strong>さんと他 <?php echo (intval($item['nice_count']) - 1);?>人があなたの回答をいいね！しました。
                        <?php } else {?>
                            <strong><?php echo $item['user_name'] ?></strong> さんがあなたの回答をいいね！しました。
                        <?php } ?>
                <?php } ?>
            </p>
        </div>
        <div class="date">
                <span>  
                <?php  echo $this->Common->dateFormat($item['updated'])?>
                </span>
         </div>
    </a>
</li>