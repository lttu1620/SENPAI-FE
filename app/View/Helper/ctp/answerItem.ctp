<li>
    <div class="avatar"><img src="<?php echo $item['user_image_url']; ?>" alt="#"></div>
    <div class="content">
        <div class="title"><?php echo $item['user_name']; ?></div>
        <div class="des"><?php echo nl2br($item['content']); ?></div>
        <?php
        foreach ($item['medias'] as $media) {
            ?>
            <div class="map">
                <?php
                if ($media['type'] == 'image') {
                    $img = $media['media_url'];
                    $size =  Configure::read('Config.imageDefaultSize');
                    $parts = explode('.', $img);
                    $last = array_pop($parts);
                    $parts = array(implode('.', $parts), $last);
                    $img = $parts[0] . $size .'.'. $last;
                    echo '<img data-src="' . $img . '"  class="lazy-image" src="'. $this->Html->webroot('img/loading.gif').'">';
                } else {
                    ?>
                    <video controls>
                        <source src="<?php echo $media['media_url']; ?>">
                        Your browser does not support the video tag.
                    </video>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
        <div class="bottom">
            <div class="answer-report">
            <div class="view-report">
                <div class="box-report">
                    <a href="javascript:;" data-event="popover"><div class="ico_three-point"></div></a>
                    <a class="popover btn-report" href="javascript:;"
                       data-event="report"
                       data-type="popoverbox"
                       title="<?php echo __('Report violation') ?>"
                       data-href="<?php echo $this->Html->url(array('controller'=>'ajax','action'=>'report',$item['question_id'],$item['id']))?>"></a>
                </div>
            </div>


            </div>
            <?php if (intval($item['nice_count']) > 0) { ?>
                <div class="ico_like on"></div>
            <?php } else { ?>
                <div class="ico_like off"></div>
            <?php } ?>
            <div class="box-like">
                <span><?php echo intval($item['nice_count']); ?></span>
                <?php if ($this->Session->check('Auth.User')) { ?>
                    <?php if (!$item['is_like']) { ?>
                        <span class="nice">
                            <a href="javascript:;" data-action="answer-like" data-flag="1"
                               data-id="<?php echo $item['id']; ?>"><?php echo __('Like') ?></a>
                        </span>
                    <?php } else { ?>
                        <span class="nice">
                            <a href="javascript:;" data-action="answer-like" data-flag="0"
                               data-id="<?php echo $item['id']; ?>"><?php echo __('Unlike') ?></a>
                        </span>
                    <?php } ?>
                <?php } else { ?>
                    <span class="nice">
                        <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'login')) ?>"><?php echo __('Like') ?></a>
                    </span>
                <?php } ?>
            </div>
        </div>
    </div>
</li>