<?php
/**
 * search action
 * 
 * @package View/Questions
 * @created 2015-03-16
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
?>
<?php
$jsLinks = array(
    'likeUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'likequestion')),
    'favoriteUrl' => $this->Html->url(array('controller' => 'ajax', 'action' => 'favoritequestion')),
);
$this->Html->scriptStart(array('inline' => false));
echo 'var urls = ' . json_encode($jsLinks) . ';';
$this->Html->scriptEnd();

$this->Html->script('actions', array('inline' => false));
$active_tab = isset($this->request->query['joined_user_id']) ? 1 : (isset($this->request->query['user_id']) ? 2 : 0);

$query = array();
if(isset($this->request->query['category_id']))
    $query['category_id'] = $this->request->query['category_id'];
if(isset($this->request->query['keyword']))
    $query['keyword'] = $this->request->query['keyword'];
$query1 = $query;
$query1['joined_user_id'] = $this->Session->read('Auth.User')->id;
$query2 = $query;
$query2['user_id'] = $this->Session->read('Auth.User')->id;
?>
<div id="headingPage">
    <a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'search','?' => $query)) ?>"><span
            class="<?php if($active_tab == 0) echo 'salmon active'; else echo 'blueLight' ; ?>"><?php echo __('All')?></span></a>
    <a href="<?php echo $this->Html->url(
        array(
            'controller' => 'questions',
            'action' => 'search',
            '?' => $query1
        )
    )?>"><span class="<?php if($active_tab == 1) echo 'salmon active'; else echo 'blueLight' ; ?>"><?php echo __('Joined questions')?></span></a>
    <a href="<?php echo $this->Html->url(
        array(
            'controller' => 'questions',
            'action' => 'search',
            '?' => $query2
        )
    )?>"><span
            class="<?php if($active_tab == 2) echo 'salmon active'; else echo 'blueLight' ; ?>"><?php echo __('My questions')?></span></a>
</div>
<div id="itemList01">
    <ul>
        <?php
        foreach ($data as $item) {
            echo $this->Item->questionItem($item);
        }
        ?>
        <li>
            <div id="pagination">
                <?php
                echo $this->Paginate->render($total, $limit);
                ?>
            </div>
        </li>
    </ul>
    <div class="animation_image" style="display:none" align="center">
        <img src="<?php echo $this->Html->webroot('img/loading.gif')?>">
    </div>
</div>