<div id="catList_02">
    <ul>
        <?php foreach ($categories as $item): ?>
            <li class="bGreen category-item" style="border-color:<?php echo $item['color'] . '!important'?> " data-href="<?php echo($this->Html->url('/search?category_id='.$item['id']))?>">
                <a href="<?php echo($this->Html->url('/search?category_id='.$item['id']))?>">
                        <div class="thumbnail" style="background-color:<?php echo $item['color'] . '!important'?>">
                            <img src="<?php echo $item['image_url'];?>" alt="#">
                        </div>
                        <div class="text"><?php echo $item['name'];?></div>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>