<?php

/**
 * 
 * Events for application
 * @package Lib
 * @created 2015-03-31
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
App::uses('CakeEventListener', 'Event');

class AppEventListener implements CakeEventListener {

    public function __construct() {
        
    }

    public function implementedEvents() {
        return array(            
            'Admin.beforeLogin' => 'adminBeforeLogin',
            'Admin.afterLogin' => 'adminAfterLogin',
            'User.beforeLogin' => 'userBeforeLogin',
            'User.afterLogin' => 'userAfterLogin',
        );
    }
    
    public function userBeforeLogin($event) {
        
    }
    
    public function adminBeforeLogin($event) {
        
    }
    
    public function adminAfterLogin($event) {
        $data = $event->data;
        AppLog::info("Admin {$data['login_id']} logined at " . date('Y-m-d H:i:s'), __METHOD__);
    }

    public function userAfterLogin($event) {
        $data = $event->data;
        AppLog::info("User {$data['email']} logined at " . date('Y-m-d H:i:s'), __METHOD__);
    }

}
